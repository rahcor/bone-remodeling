// Rotina que calcula a densidade mineral ossea considerando o delay de mineralizacao
#ifndef _BMD_H
#include "00_var_common.h"
#define	_BMD_H

void bmd (void)
{
	if (tempo_atual == 0 ) {
		//C�lculo da BMD obtida diretamente da porosidade
		if ( por_i<0.3 )			//Se a porosidade for menor que 30% - Modelo linear
			bmd_i = -15.754*(por_i*100) + 1384;
		else						//Se a porosidade for maior que 30% - Hip�tese assumida
			bmd_i = 0.03906122449*(por_i*100)*(por_i*100) - 18.09767347 *(por_i*100) + 1419.155102;
			
		bmd_ref_max = bmd_i;	//Inicializa��o do bmd_ref_max
		bmd_ini = bmd_i;	//Atribuicao do valor inicial de bmd
	}
	
	//C�lculo da BMD obtida diretamente da porosidade
	if ( por_i<0.3 )			//Se a porosidade for menor que 30% - Modelo linear
		bmd_ref = -15.754*(por_i*100) + 1384;
	else						//Se a porosidade for maior que 30% - Hip�tese assumida
		bmd_ref = 0.03906122449*(por_i*100)*(por_i*100) - 18.09767347 *(por_i*100) + 1419.155102;

	//Verificacao de pico de BMD obtido da porosidade
	if ( bmd_ref > bmd_ref_max ) { 	//Se o bmd de ref atual � o maior obtido
		bmd_ref_max = bmd_ref;		//		atualiza a ref maxima
		coef_reta_min = (bmd_ref_max - bmd_i) / tmin;	//	e recalcula o coeficiente da reta de mineralizacao
		}

	//Calculo do BMD atual: comparacao entre o valor de ref e o valor da reta
	if ( bmd_ref > bmd_i ) {		//Se a bmd da porosidade eh maior que a bmd anterior
		bmd_i = bmd_i + coef_reta_min * h;	//		utiliza equa��o da reta de mineralizacao para calcular novo bmd.
		if ( bmd_i > bmd_ref ) {		//Se o novo bmd calculado eh maior que o bmd de referencia
			bmd_i = bmd_ref;			//		eh usado o menor valor
			bmd_ref_max = 0;
			}
		}
	else {						//Se a bmd da porosidade eh menor ou igual a bmd anterior
		bmd_i = bmd_ref;		//		utiliza a bmd da porosidade
		bmd_ref_max = 0;
		}
	
	//Calculo da diferenca para o bmd inicial em porcentagem
	bmd_delta_nrm_i = (100*bmd_i - 100*bmd_ini)/bmd_ini;
}
#endif
