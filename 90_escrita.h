// Rotina que realiza os procedimentos para escrita em arquivo dos valores calculados no modelo
#ifndef _ESCRITA_H
#include <stdio.h>
#include "00_var_common.h"
#include "60_estimulo_mec.h"
#define	_ESCRITA_H

#define TRUE	1
#define FALSE	0

void ini_arquivos_saida (void)
{
	//Abertura dos arquivos de escrita e escrita dos cabecalhos
		//Arquivo	com popula��es absolutas e normalizadas
		ofp_pop = fopen("out_10_populacoes.txt", "w");
		fprintf (ofp_pop, "Tempo \t\t C_OBp \t\t C_OBa \t\t C_OCa \t\t C_OBp_norm \t C_OBa_norm \t C_OCa_norm \t dC_OBp \t dC_OBa \t dC_OCa\n");	//Escreve cabecalho do arquivo output
	
		//			com fra�oes de volume, bmd e w
		ofp_pim = fopen("out_20_param_mec_med.txt", "w");
		fprintf (ofp_pim, "Tempo \t\t f_vas \t\t porosidade \t Vcel/Vvas \t BMD \t\t w \t\t df_vas \t eps_11 \t esp_22 \t eps_33 \t sig_11 \t sig_22 \t sig_33\n");	//Escreve cabecalho do arquivo output
	
		//			com matriz de rigidez c_meso
		ofp_mtr = fopen("out_30_rigidez.txt", "w");
		fprintf (ofp_mtr, "Tempo \t\t c_cort_11 \t c_cort_12 \t c_cort_13 \t c_cort_14 \t c_cort_15 \t c_cort_16 \t c_cort_22 \t c_cort_23 \t c_cort_24 \t c_cort_25 \t c_cort_26 \t c_cort_33 \t c_cort_34 \t c_cort_35 \t c_cort_36 \t c_cort_44 \t c_cort_45 \t c_cort_46 \t c_cort_55 \t c_cort_56 \t c_cort_66\n");	//Escreve cabecalho do arquivo output
	
		//			com par�metros de remodelagem
		ofp_rmd = fopen("out_40_param_remod.txt", "w");
		fprintf (ofp_rmd, "Tempo \t\t Pi_mec_act_obp\t Beta_rkl_mech \t TGFb \t\t Pi_Tb_act_Obu \t Pi_Tb_rep_Obp \t Pi_Tb_act_oca \t C_PTH_PTHR \t Pi_pth_act_ob \t Pi_pth_rep_ob \t Beta_opg_max \t OPG \t\t RKLmax \t RKLtot \t C_RKL \t\t C_RKL_RK \t Pi_rkl_act_ocp\n");		//Escreve cabecalho do arquivo output

		//			com sumario dos par�metros do metodo itlin de OPG-RANKL
		ofp_ork = fopen("out_41_itlin_OPG_RKL.txt", "w");
		fprintf (ofp_ork, "Tempo \t\t OPG \t\t C_RKL \t\t no_its \t erro \t\t erro_tgt\n");		//Escreve cabecalho do arquivo output

		//Arquivo com parametros de runge-kutta
		ofp_mrk = fopen("out_42_runge_kutta_ode45.txt", "w");
		fprintf (ofp_mrk, "Tempo \t\t conh \t\t h_final \t k[0] \t\t k[1] \t\t k[2] \t\t k[3] \t\t k[4] \t\t k[5] \t\t k[6] \t\t DeltaOBp \t l[0] \t\t l[1] \t\t l[2] \t\t l[3] \t\t l[4] \t\t l[5] \t\t l[6] \t\t DeltaOBa \t m[0] \t\t m[1] \t\t m[2] \t\t m[3] \t\t m[4] \t\t m[5] \t\t m[6] \t\t DeltaOCa \t p[0] \t\t p[1] \t\t p[2] \t\t p[3] \t\t p[4] \t\t p[5] \t\t p[6] \t\t Deltafvas \t erroOBp \t erroOBa \t erroOCa \t errofvas \t er_tgt_fvas \t errogrande? \t tempo_final_aux\t repeticao\n");
		//ofp_mrk = fopen("out_42_runge_kutta.txt", "w");
		//fprintf (ofp_mrk, "Tempo \t\t k[0] \t\t k[1] \t\t k[2] \t\t k[3] \t\t DeltaOBp \t l[0] \t\t l[1] \t\t l[2] \t\t l[3] \t\t DeltaOBa \t m[0] \t\t m[1] \t\t m[2] \t\t m[3] \t\t DeltaOCa \t p[0] \t\t p[1] \t\t p[2] \t\t p[3] \t\t Deltafvas\n");
}

void ini_arquivos_saida_pos (void)
{
	//Abertura dos arquivos de escrita e escrita dos cabecalhos para pos processamento
		//Arquivo com freq de aquisicao total
		ofp_pos_tot = fopen("out_pos_total_h-erros-def.txt", "w");
		fprintf (ofp_pos_tot, "Tempo \t\t h_final \t\t erroOBp \t erroOBa \t erroOCa \t errofvas\n");	//Escreve cabecalho do arquivo output
	
		//Arquivo com aquisicao parcial do tempo
		Dia_ini_aqpar = DiaP_ini;	//Definicao do dia inicial para a aquisicao parcial
		int_dias_aq = 7;			//Intervalo de tempo para aquisicao total dos dados desse arquivo
		ofp_pos_par = fopen("out_pos_intervalo_cell-bio-bmd-mec.txt", "w");
		fprintf (ofp_pos_par, "Tempo \t\t C_OBp \t\t C_OBa \t\t C_OCa \t\t Pi_mec_act_obp\t Beta_rkl_mech \t TGFb \t\t Pi_Tb_act_Obu \t Pi_Tb_rep_Obp \t Pi_Tb_act_oca \t C_PTH_PTHR \t Pi_pth_act_ob \t Pi_pth_rep_ob \t Beta_opg_max \t OPG \t\t RKLmax \t RKLtot \t C_RKL \t\t C_RKL_RK \t Pi_rkl_act_ocp\t DeltaBMDnrm \t w \t\t eps_22 \t eps_33\n");	//Escreve cabecalho do arquivo output
	
		//Arquivo com aquisicao por dia (frequencia reduzida)
		dia_lfaq_ref = 1234;	//Inicializacao do dia de referencia
		vet_info[47] = 0; vet_info[48] = 0; vet_info[49] = 0; vet_info[50] = 0; //Inicializacao das variaveis de erro acumulado
		KSc_OBp = 0; KSc_OBa = 0; KSc_OCa = 0; KSc_fvas = 0;				//Inicializa��o dos Corretores do algoritmo de soma de Kahan
		ofp_pos_lfaq = fopen("out_pos_lfaq_bmd.txt", "w");
		fprintf (ofp_pos_lfaq, "Tempo \t\t DeltaBMDnrm \t erracmOBp \t erracmOBa \t erracmOCa \t erracmfvas \t w\n");	//Escreve cabecalho do arquivo output
}

void escreve_linha (void)
{
	int con;
	
		//Arquivo com popula��es absolutas e normalizadas
		fprintf (ofp_pop, "%.5lf \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E\n", vet_esc[0], vet_esc[1], vet_esc[2], vet_esc[3], vet_esc[4], vet_esc[5], vet_esc[6], vet_esc[48], vet_esc[49], vet_esc[50]);
		
		//Arquivo com fra�oes de volume, bmd, w, estado tensao-deformacao
		fprintf (ofp_pim, "%.5lf \t %.5E \t %.5E \t %.5E \t %.7E \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E\n", vet_esc[0], vet_esc[7], vet_esc[8], vet_esc[52], vet_esc[9], vet_esc[10], vet_esc[51], vet_esc[53], vet_esc[54], vet_esc[55], vet_esc[56], vet_esc[57], vet_esc[58]);
		
		//Arquivo com matriz de rigidez c_cort
		fprintf (ofp_mtr, "%.5lf", vet_esc[0]);
		for (con=0; con<21 ; con++)
			fprintf (ofp_mtr, " \t %.5E", vet_esc[11+con]);
		fprintf (ofp_mtr, "\n");
		
		//Arquivo com par�metros de remodelagem
		fprintf (ofp_rmd, "%.5lf", vet_esc[0]);
		for (con=32; con<48 ; con++)
			fprintf (ofp_rmd, " \t %.5E", vet_esc[con]);
		fprintf (ofp_rmd, "\n");
		
		//Arquivo com par�metros do metodo itlin para OPG-RKL
		fprintf (ofp_ork, "%.5lf \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E\n", vet_esc[0], vet_info[0], vet_info[1], vet_info[2], vet_info[3], vet_info[4]);
}

void escreve_linha_mrk (void)
{
	int con;
		
		//Arquivo com informa��es sumario do m�todo de rkf45
		fprintf (ofp_mrk, "%.5lf \t %.5lf \t %.8lf", vet_esc[0], vet_info[43], vet_info[44]);
		//fprintf (ofp_mrk, "%.5lf", vet_esc[0]);
		for (con=5; con<42 ; con++)
			fprintf (ofp_mrk, " \t %.5E", vet_info[con]);
		(vet_info[42]==TRUE)? fprintf(ofp_mrk," \t YES"):fprintf(ofp_mrk," \t NO");
		fprintf (ofp_mrk, " \t\t %.5E", vet_info[45]);
		fprintf (ofp_mrk, " \t\t %.5E", vet_info[46]);
		fprintf (ofp_mrk, "\n");
}

void escreve_linha_pos (void)
{
	//Escreve no arquivo de aquisicao total
	fprintf (ofp_pos_tot, "%.7lf \t %.8E \t %.5E \t %.5E \t %.5E \t %.5E\n", vet_esc[0], vet_info[44], vet_info[37], vet_info[38], vet_info[39], vet_info[40]);
	
	//Escreve no arquivo de aquisicao de intervalo de tempo
	if ( tempo_atual >= Dia_ini_aqpar && tempo_atual <= (Dia_ini_aqpar+int_dias_aq) ) {	
		fprintf (ofp_pos_par, "%.7lf \t %.5E \t %.5E \t %.5E", vet_esc[0], vet_esc[1], vet_esc[2], vet_esc[3]);
		int con;
		for (con=32; con<48 ; con++)
			fprintf (ofp_pos_par, " \t %.5E", vet_esc[con]);
		fprintf (ofp_pos_par, " \t %.5E \t %.5E \t %.5E \t %.5E\n", vet_esc[59], vet_esc[10], vet_esc[54], vet_esc[55]);	
	}
	
	//Escreve no arquivo de aquisicao a taxa reduzida - tx diaria desde que h menor que 1 dia
	double tempo_atual_int;
	modf(tempo_atual, &tempo_atual_int);	//Funcao modf para identificar a parte inteira do tempo_atual
	if ( tempo_atual_int != dia_lfaq_ref ) {
		fprintf (ofp_pos_lfaq, "%.7lf \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E \t %.8E\n", vet_esc[0], vet_esc[59], vet_info[47], vet_info[48], vet_info[49], vet_info[50], vet_esc[10]);
		dia_lfaq_ref = tempo_atual_int;
	}
}
	
void fecha_arq_saida (void)
{
	//Fechamento dos arquivos de saida
		//Arquivo	com popula��es absolutas e normalizadas
		fclose(ofp_pop);
		//			com fra�oes de volume, bmd e w
		fclose(ofp_pim);
		//			com matriz de rigidez c_meso
		fclose(ofp_mtr);
		//			com par�metros de remodelagem
		fclose(ofp_rmd);
		//
		fclose(ofp_ork);
		//
		fclose(ofp_mrk);
		fclose(ofp_pos_tot);
		fclose(ofp_pos_par);
		fclose(ofp_pos_lfaq);
}

////FUNCOES DE ARMAZENAMENTO NO VETOR DE ESCRITA
void vet_esc_populacoes (void)
{
	vet_esc[0] = tempo_atual;
	vet_esc[1] = C_OBp_i;
	vet_esc[2] = C_OBa_i;
	vet_esc[3] = C_OCa_i;
	vet_esc[4] = C_OBp_n_i;
	vet_esc[5] = C_OBa_n_i;
	vet_esc[6] = C_OCa_n_i;
}

void vet_esc_frac_volume (void)
{
	vet_esc[7] = f_vas_i;
	vet_esc[8] = por_i;
	vet_esc[52]= Rv;
}

void vet_esc_bmd (void)
{
	vet_esc[9] = bmd_i;
	vet_esc[59]= bmd_delta_nrm_i;
}

void vet_esc_estimulo_mec (void)
{
	vet_esc[10] = w;
	vet_esc[53] = epsilon[conv_ind(1,1)-1];
	vet_esc[54] = epsilon[conv_ind(2,2)-1];
	vet_esc[55] = epsilon[conv_ind(3,3)-1];
	vet_esc[56] = sigma[conv_ind(1,1)-1];
	vet_esc[57] = sigma[conv_ind(2,2)-1];
	vet_esc[58] = sigma[conv_ind(3,3)-1];
}

void vet_esc_matriz (void)	//Armazena todos valores (sem valores simetricos) da matriz de rigidez do osso no vetor de dados a serem escritos	
{
	int con=0;
	for (con=0 ; con<21 ; con++)
			vet_esc[11+con] = c_cort[con]*1.0E-9;
}

void vet_esc_biomecanica (void)
{
	vet_esc[32] = Pi_mech_act_obp;
	vet_esc[33] = Beta_rkl_mech;

}

void vet_esc_implicitas (void)
{
	vet_esc[34] = TGFb;
	vet_esc[35] = Pi_Tb_act_Obu;
	vet_esc[36] = Pi_Tb_rep_Obp;
	vet_esc[37] = Pi_Tb_act_oca;
	vet_esc[38] = C_PTH_PTR;
	vet_esc[39] = Pi_pth_act_ob;
	vet_esc[40] = Pi_pth_rep_ob;
	vet_esc[41] = Beta_opg_max;
	vet_esc[42] = OPG;
	vet_esc[43] = RKLmax;
	vet_esc[44] = RKLtot;
	vet_esc[45] = C_RKL;
	vet_esc[46] = C_RKL_RK;
	vet_esc[47] = Pi_rkl_rk_act_ocp;

}

void vet_esc_dinamica (void)
{
	vet_esc[48] = dC_OBp;
	vet_esc[49] = dC_OBa;
	vet_esc[50] = dC_OCa;
	vet_esc[51] = df_vas;
}

void vet_esc_model_par (void)
{
	vet_esc_frac_volume();		//Armazena os valores das fra��es de volume no vetor de dados a serem escritos. (90_escrita.h)
	vet_esc_bmd(); 				// (90_escrita.h)
	vet_esc_matriz();			//Armazena todos valores (sem simetricos) da matriz de rigidez do osso cortical no vetor de dados a serem escritos. (90_escrita.h)
	vet_esc_estimulo_mec();		// (90_escrita.h)
		//Armazena em vetor os seguintes par�metros: id_tempo, Pop celular absoluta e normalizada, par�metros das equa��es impl�citas
	vet_esc_populacoes();		// (90_escrita.h)
	vet_esc_biomecanica();		// (90_escrita.h)
	vet_esc_implicitas();		// (90_escrita.h)
	vet_esc_dinamica();			// (90_escrita.h)
}

#endif
