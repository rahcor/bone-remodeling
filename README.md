# Bone remodeling model program

This repository contains the source code for solving the mechanobiological bone remodeling model published on the paper: https://doi.org/10.1115/1.4044094 .

Inputs for the case studies provided on the paper are also given here.
