#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "00_var_common.h"
#include "12_modelo.h"
#include "20_leitura.h"
#include "22_atribuicao.h"
#include "24_calibracao.h"
#include "30_perturbacao.h"
#include "60_estimulo_mec.h"
#include "80_runge_kutta.h"
#include "80_runge_kutta_dopri.h"
#include "90_escrita.h"

int main (void)
{
tempo_atual=0;
	//Declar��o das vari�veis em '00_var_common.h'
	leitura();						//Leitura das configuracoes do programa, das constantes dos modelos e das perturbacoes a partir dos arquivos de input (20_leitura.h)
	ins_param_calc();				//Inser��o dos par�metros calculados no vetor de par�metros (20_leitura.h) 
	atribuicao_config(vet_ctes);	//Atribuicao dos valores das configs. do vetor 'vet_ctes' para as variaveis globais. (22_atribuicao.h)
	atribuicao_ctes(vet_ctes);		//Atribuicao dos valores das constantes do vetor 'vet_ctes' para as variaveis globais e Calculo das constantes dependentes. (22_atribuicao.h)

	C_OBp_i = C_OBp_ini;
	C_OBa_i = C_OBa_ini;
	C_OCa_i = C_OCa_ini;
    f_vas_i = por_ini - f_lac;		//f_vas_i = f_vas_ini;	
    
	ini_arquivos_saida();			//Abertura dos arquivos de escrita e escrita dos cabecalhos. (90_escrita.h)	
	ini_arquivos_saida_pos();	    

	flagesc=1;						//Instru��o de escrita: 1-habilita escrita dos resultados do metodo itlin de OPG_RKL da calibracao (ver 72_itlin_opg_rkl.h)
	//flagesc = 0;
	calibracao ();					//Calibra��o de parametros para obten��o de regime permanente (24_calibracao.h)
	atribuicaoinv_calc (vet_ctes);	//Armazena em vet_ctes os valores calibrados (22_atribuicao.h)

	if (status_ctes != SEM_PERTURB) {
		monta_vet_pert(); //Montagem do vetor de constantes perturbadas (30_pertubacao.h)
		con_rept = 1;
	}
	
	printf ("\nPressione qualquer tecla para iniciar a solucao do modelo...\n\n");
	getchar();
	
	tempo_atual=0;
	while (tempo_atual<tempo_final+h)	//La�o para realizar as simula��es do modelo de remodelagem a cada passo de tempo simulado
		{
			perturbacao ();		//Realizacao das perturba��es (30_perturbacao.h)
			
			//flagesc=2;			//Instru��o de escrita: 2-habilita escrita dos resultados do metodo itlin de OPG_RKL do 'tempo_atual' (ver 72_itlin_opg_rkl.h)
			modelo ();			//Solu��o das equa��es dos modelos no 'tempo_atual' (12_modelo.h)	

			vet_esc_model_par();	//Armazena os valores dos par�metros do modelo no vetor de dados a serem escritos (90_escrita.h)
			escreve_linha();		//Escreve em arquivo os valores armazenados no vetor de dados a serem escritos (90_escrita.h)			
			printf ("\r Andamento = %.2lf %% = dia %.1lf de %.0lf\r", (tempo_atual/tempo_final)*100, tempo_atual, tempo_final);	//Mostra na tela o andamento da simulacao
			
			//Solu��o das edos	
			flagesc=0;			//Instru��o de escrita: 0-desabilita escrita dos resultados do metodo itlin de OPG_RKL (ver 72_itlin_opg_rkl.h);		
			dopri5();    		//Solu��o do sistema de edos de popula��o e volume - Runge-Kutta-Dorman-Prince de passo adaptativo (80_runge_kutta_dopri.h)    
			//runge_kutta();	//Solu��o do sistema de edos de popula��o e volume - Runge-Kutta (80_runge_kutta.h)
			escreve_linha_mrk(); //Escreve em arquivo os valores relativos ao metodo numerico armazenados no vetor de dados a serem escritos (90_escrita.h)
			
			escreve_linha_pos(); //Escreve os dados nos arquivos que serao pos-processados manualmente
			
		}//Fim dos c�lculos de id_tempo

	fecha_arq_saida ();			//Fecha os arquivos de saida (90_escrita.h)
	printf("\n\n____________Calculos concluidos!____________\n\n");
	
	return (0);
}




