//Variaveis perturbadas e seus valores//
//Tipos disponiveis: desl=desligado, fracao=multiplica constante pela perturba��o, valor=substitui valor da constante
//No estado de tensao e deformacao, deve-se perturbar somente tensao (sig) ou somente deformacao (eps)
Tipo	Valor		Nota��o		Unidade	Descri��o
desl	5.000E-05	P_TGFb		pM
desl	1.000E+03	P_PTH		pM
valor	2.000E+05	P_OPG		pM	>>>valor
desl	10.00E-00	P_RANKL		pM
desl	0.250		Tmv_TGFb	
desl	1.000		D_TGFb		
desl	0.250		DOBu				
desl	2.718		DOCp		dia-1	Taxa de diferencia��o das c�lulas precursoras dos osteoclastos
desl	10.000		AOBa		dia-1	Taxa de apoptose de osteoblastos ativos
desl	5.000		AOCa		dia-1	Taxa de apoptose dos osteoclastos
desl	0.100		p_opg_ob	dia-1	Taxa de produ��o de OPG (Por pre-osteoblastos ou osteoblastos ativos)
desl	1.820E-01	alfa		pM	Conte�do de TGF-b armazenado na matriz �ssea
desl	5.00		Beta_PTH	pM.dia-1	Taxa de produ��o de PTH sist�mico
desl	00.00E-00	eps_1		Pa	Componente sig_11 (radial) da matriz tens�o do osso cortical
desl	1378.00E-06	eps_2		Pa	Componente sig_22 (angular) da matriz tens�o do osso cortical
desl	-1675.00E-06	eps_3		Pa	Componente sig_33 (longitudinal) da matriz tens�o do osso cortical
desl	00.00E+00	eps_4		Pa	Componente sig_12 da matriz tens�o do osso cortical
desl	00.00E+00	eps_5		Pa	Componente sig_23 da matriz defortens�oma��o do osso cortical
desl	00.00E+00	eps_6		Pa	Componente sig_31 da matriz tens�o do osso cortical
