Instrucoes
-Esse arquivo texto eh o arquivo de input do programa.
-As notacoes nao devem ser alteradas.
-A unidade e descricao sao somente informativos. portanto nao sao utilizados como input do programa.
-A ordem dos termos das matrizes de rigidez e do vetor epsilon devem ser mantidas.
//Parametros de configuracao//
Valor		Nota��o		Unidade	Descri��o
2.000E-01	h		dia	Passo de solu��o inicial no m�todo de Runge-Kutta. 0,008=10min
1.000E-04	h_min		dia	Passo de solu��o m�nimo no m�todo de Runge-Kutta-Dorman-Prince
10.00E-00	h_max		dia	Passo de solu��o m�ximo no m�todo de Runge-Kutta-Dorman-Prince
1.000E-10	errotgt		-	Erro desejado para os processos iterativos em geral
1.250E+02	vol_tgt		mm^3	Volume observado
2000.0E+00	it_max		-	Numero maximo permitido de iteracoes nos m�todos em geral
140.0E+00	tempo_final	dia	Per�odo de tempo total simulado
20.00E+00	DiaP_ini	dia	Dia inicial da perturbacao	
80.00E+00	DiaP_fim	dia	Dia final da perturbacao
1.000E-01	deltat_pert	dia	Periodo de tempo de aplicacao da rampa de perturbacao das constantes
1.000E-01	deltat_rest	dia	Periodo de tempo de aplicacao da rampa de restauracao das constantes
0.000E+00	freq_pert	dia	Intervalo de tempo entre aplica��es consecutivas de pertuba��o. Se 0, ent�o a perturba��o � constante.
2700.0E+00	repeticoes	-	Numero de repeticoes no exercicio ou perturbacao
0.08E-00	por_ini		-	Porosidade �ssea inicial
0.02E+00	f_lac		-	Fra��o volum�trica de lacunas no cortical
30.0E+00	tmin		dia	Intervalo de tempo de mineraliza��o completa do tecido novo
//Constantes do modelo//
Valor		Nota��o		Unidade	Descri��o
2.1107E-01	AOBa		dia-1	Taxa de apoptose de osteoblastos ativos
7.000E-01	AOCa		dia-1	Taxa de apoptose dos osteoclastos
1.000E-01	aPOBp		-	Fra��o de prolifera��o de preosteoblastos	
7.282E-04	C_OBa_ini	pM	Popula��o inicial de osteoblastos ativos
7.734E-04	C_OBp_ini	pM	Popula��o inicial de pr�-osteoblastos
1.307E-05	C_OBu		pM	Popula��o de precursores de osteoblastos
9.127E-04	C_OCa_ini	pM	Popula��o inicial de osteoclastos ativos
7.863E-04	C_OCp		pM	Popula��o de precursores de osteoclastos
2.250E+00	C_OPG_0		pM	Concentra��o s�rica normal de OPG
2.000E+01	R_OPG_max	pM	Concentra��o m�xima poss�vel de OPG
7.863E-01	C_RANK		pM	Concentra��o fixa de RANK
1.000E-01	C_RKL_0		pM	Concentra��o s�rica normal de RANKL
1.920E+02	C_TGFb_0	pM	Concentra��o s�rica normal de TGFb
6.759E-02	Hpth_rep_ob	pM	Coeficiente de repress�o relacionado a liga��o de PTH nos precursores dos osteoblastos e osteoblastos ativos regulando o OPG	
5.882E-04	Ka_RL_OPG	pM-1	Constante de associa��o RANKL-OPG	
1.000E-02	Ka_RL_R		pM-1	Constante de associa��o RANKL-RANK
5.263E-04	Ka_PTH_R	pM-1	Constante de associa��o PTH-PTH1R
2.000E+00	Kres		pM-1.dia-1	Taxa relativa de formacao de osso
2.000E+04	N_PTHR_OB	-	N�mero de receptores de PTH na superf�cie de cada OBa ou OBp
2.703E+06	N_RKL_OB	-	N�mero m�ximo de receptores de RANKL na superf�cie de cada osteoblastos ativos ou pr�-osteoblastos	
2.089E+04	p_opg_ob	dia-1	Taxa de produ��o de OPG (Por pre-osteoblastos ou osteoblastos ativos)	
0.010E+00	Pi_pth_act_ob_0	-	Coeficiente de ativa��o relacionado a liga��o de PTH nos precursores dos osteoblastos e osteoblastos ativos regulando o RANKL	
0.200E+00	Pi_rkl_rk_act_ocp_0 -	Coeficiente de equil�brio para liga��o (dissocia��o) do RANKL no RANK	
0.400E+00	Pi_Tb_act_Obu_0	-	Coeficiente de ativa��o relacionado a liga��o de TGF-b nos osteoblastos acometidos e osteoclastos	
0.200E+00	Pi_Tb_act_Oca_0	-	Coeficiente de ativa��o relacionado a liga��o de TGF-b nos osteoblastos acometidos e osteoclastos	
0.750E+00	Pi_Tb_rep_Obp_0	-	Coeficiente de repress�o relacionado a liga��o de TGF-b nos precursores dos osteoblastos	
2.630E+00	Tmv_PTH		min	Tempo de meia vida da mol�cula de PTH
2.400E+02	Tmv_RKL		min	Tempo de meia vida da mol�cula de RANKL
1.086E+02	Tmv_TGFb	min	Tempo de meia vida da mol�cula de TGF-b
9.744E+02	Beta_PTH	pM.dia-1	Taxa de produ��o de PTH sist�mico	
0.500E+00	Pi_mech_ref	-	Valor m�nimo de referencia da Funcao de ativacao da proliferecao de pre-osteoblasto por estimulo mecanico
1.000E+00	Pi_mech_max	-	Valor m�ximo da Funcao de ativacao da proliferecao de pre-osteoblasto por estimulo mecanico
1.000E+03	kappa1		pM.dia-1 Coeficiente de inclinacao da reta de descida na funcao de producao de RANKL por estimulo mec�nico	
1.000E+01	kappa2		pM.dia-1 Coeficiente de inclinacao da reta de subida na funcao de producao de RANKL por estimulo mec�nico	
6.133E+02	w_r1		Pa	Estimulo mecanico inferior entre regiao de reabsorcao e zona morta
3.450E+04	w_f1		Pa	Estimulo mecanico de inicio da subida ao patamar de formacao
9.583E+04	w_f2		Pa	Estimulo mecanico de inicio do patamar de formacao
2.453E+05	w_f3		Pa	Estimulo mecanico de fim do patamar de formacao e inicio da reta de descida
3.833E+05	w_f4		Pa	Estimulo mecanico do fim da descida do patamar de formacao
3.833E+05	w_r2		Pa	Estimulo mecanico superior entre regiao de zona morta e reabsorcao
1.990E+01	c_bm_11		GPa	Componente c_1111 do tensor de rigidez da matriz �ssea
1.100E+01	c_bm_12		GPa	Componente c_1122 do tensor de rigidez da matriz �ssea
1.110E+01	c_bm_13		GPa	Componente c_1133 do tensor de rigidez da matriz �ssea
0.000E+00	c_bm_14		GPa	Componente c_1112 do tensor de rigidez da matriz �ssea
0.000E+00	c_bm_15		GPa	Componente c_1123 do tensor de rigidez da matriz �ssea
0.000E+00	c_bm_16		GPa	Componente c_1131 do tensor de rigidez da matriz �ssea
2.240E+01	c_bm_22		GPa	Componente c_2222 do tensor de rigidez da matriz �ssea
1.180E+01	c_bm_23		GPa	Componente c_2233 do tensor de rigidez da matriz �ssea
0.00E+00	c_bm_24		GPa	Componente c_2212 do tensor de rigidez da matriz �ssea
0.00E+00	c_bm_25		GPa	Componente c_2223 do tensor de rigidez da matriz �ssea
0.00E+00	c_bm_26		GPa	Componente c_2231 do tensor de rigidez da matriz �ssea
3.060E+01	c_bm_33		GPa	Componente c_3333 do tensor de rigidez da matriz �ssea
0.00E+00	c_bm_34		GPa	Componente c_3312 do tensor de rigidez da matriz �ssea
0.00E+00	c_bm_35		GPa	Componente c_3323 do tensor de rigidez da matriz �ssea
0.00E+00	c_bm_36		GPa	Componente c_3331 do tensor de rigidez da matriz �ssea
6.920E+00	c_bm_44		GPa	Componente c_1212 do tensor de rigidez da matriz �ssea
0.00E+00	c_bm_45		GPa	Componente c_1223 do tensor de rigidez da matriz �ssea
0.00E+00	c_bm_46		GPa	Componente c_1231 do tensor de rigidez da matriz �ssea
6.230E+00	c_bm_55		GPa	Componente c_2323 do tensor de rigidez da matriz �ssea
0.00E+00	c_bm_56		GPa	Componente c_2331 do tensor de rigidez da matriz �ssea
5.020E+00	c_bm_66		GPa	Componente c_3131 do tensor de rigidez da matriz �ssea
7.66667E-01	c_vas_11	GPa	Componente c_1111 do tensor de rigidez dos canais vasculares	
7.66667E-01	c_vas_12	GPa	Componente c_1122 do tensor de rigidez dos canais vasculares	
7.66667E-01	c_vas_13	GPa	Componente c_1133 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_14	GPa	Componente c_1112 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_15	GPa	Componente c_1123 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_16	GPa	Componente c_1131 do tensor de rigidez dos canais vasculares	
7.66667E-01	c_vas_22	GPa	Componente c_2222 do tensor de rigidez dos canais vasculares	
7.66667E-01	c_vas_23	GPa	Componente c_2233 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_24	GPa	Componente c_2212 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_25	GPa	Componente c_2223 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_26	GPa	Componente c_2231 do tensor de rigidez dos canais vasculares	
7.66667E-01	c_vas_33	GPa	Componente c_3333 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_34	GPa	Componente c_3312 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_35	GPa	Componente c_3323 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_36	GPa	Componente c_3331 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_44	GPa	Componente c_1212 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_45	GPa	Componente c_1223 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_46	GPa	Componente c_1231 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_55	GPa	Componente c_2323 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_56	GPa	Componente c_2331 do tensor de rigidez dos canais vasculares	
0.00E+00	c_vas_66	GPa	Componente c_3131 do tensor de rigidez dos canais vasculares

