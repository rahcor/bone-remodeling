// Rotina que calcula o volume aproximado ocupado por OBa e OCa
#ifndef _VOLUME_H
#include "00_var_common.h"
#define	_VOLUME_H

void volume (void)
{
	double	V_OBa, V_OCa;			//Volume total ocupado pelas celulas OBa e OCa, em mm^3
	double	d_OBa=20.0E-3, d_OCa=80.0E-3;	//Di�metro das c�lulas em mm
	double	NA=6.022E23;			//Numero de Avogrado;
	
	V_OBa = (4.0/3.0)*3.14159265*(d_OBa/2)*(d_OBa/2)*(d_OBa/2);	//Volume de uma esfera de di�metro 'd_OBa' em mm^3
	V_OCa = (4.0/3.0)*3.14159265*(d_OCa/2)*(d_OCa/2)*(d_OCa/2);
	
	Rv = (V_OBa*C_OBa_i + V_OCa*C_OCa_i) * NA * 1.0E-18;		//Raz�o entre volume das celulas e volume vascular 

}

#endif

