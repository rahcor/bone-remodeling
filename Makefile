#makefile -f makefile

all:

	gcc -o mainc11 10_main.c -Wall -lm -std=c11

noc11:

	gcc -o main 10_main.c -Wall -lm

debug:

	gcc -o main_debug 10_main_debug.c -Wall -lm

clean:

	rm main* out* 10_main.o 

