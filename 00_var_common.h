//Declaracao das variaveis globais
#ifndef _VAR_COMMON
#include <stdio.h>
#define _VAR_COMMON

////Analisar precis�o das variaveis para adequar tipo de variavel

//Parametros de configuracao do programa
int		Kipt;			//Tipo do Input de kres ou kform
#define iptKres  0
#define iptKform 1
int		Tipt;			//Tipo do Input de tensor de estado de tensao-deformacao
#define ipteps	0		//Input de deformacao
#define iptsig	1		//Input de tensao

//char	err[20];		//Flag de erro. Consultar ref_error_codes.txt
int		flagesc;		//Flag de escrita: 0=desligado, 1=calibra��o, 2=est�gio1do m�todo de RKF
int		nlin_max=140;	//nlin_max=numero de parametros do arquivo de input. N�o deve ser maior que o tamanho de 'vet_ctes'.
int		nlin, nlin_pert;//nlin=numero de linhas do vetor de dados de entrada; nlin_pert=numero de linhas do arquivo de input de perturbacao
struct	tipo_cte {		//Tipo de dado criado para armazenar o valor e nome da constante lida dos arquivos de input em uma mesmo �ndice/posi��o de um vetor. 'tipo' � usado no input de perturbacao.
	char	nome[20];
	float	valor;
	char	tipo[20];
};
struct tipo_cte	vet_ctes[140];				//Vetor em que serao armazenados os dados das constantes lidas do arquivo input.txt
struct tipo_cte	vet_ctes_pert[140];			//Vetor copia do vet_ctes, porem com os valores perturbados
struct tipo_cte	vet_ctes_pert_aux[140];			//Vetor copia do vet_ctes, porem com os valores perturbados
struct tipo_cte	vet_ctes_pert_lidos[140];	//Vetor em que serao armazenados os dados das constantes lidas do arquivo input_pert.txt

int		status_ctes;	//Variavel para identifica��o do status das constantes com relacao a perturbacao
#define N_PERTURBADO 1
#define RAMPA_PERT   2
#define PERTURBADO   3
#define RAMPA_REST   4
#define RESTAURADO   5
#define	SEM_PERTURB  0

#define	ITMAX	1		//Codigo referente ao 'atingimento' do numero maximo de iteracoes

//Parametros de calibracao//
//		Nota��o		Unidade		Descri��o
double	vol_tgt;	//mm^3		Volume representativo desejado
double	vol;		//mm^3		Volume do volume representativo

//Par�metros de execucao dos m�todos numericos//
//		Nota��o		Unidade		Descri��o
double	h; 			//	-		Passo de tempo da solu��o no m�todo de Runge-Kutta
double	h_min; 		//	-		Passo minimo de tempo da solu��o no m�todo de Runge-Kutta
double	h_max; 		//	-		Passo m�ximo de tempo da solu��o no m�todo de Runge-Kutta
int		erk;		//	-		Identificador do est�gio de solu��o do m�todo de Runge-Kutta. erk=1,2,3,4,... .
double	errotgt;	//	-		Erro desejado para a calibra��o da populacao inicial e de OPG-RANK nos m�todos iterativo linear de ambos
double	it_max;		//	-		Numero maximo permitido de iteracoes no metodo iterativo linear
double	tempo_atual;//	dia		Tempo em simulacao pelo modelo
double	tempo_final;//	dia		Per�odo de tempo total simulado
//Vari�veis usadas no c�lculo computacional do erro acumulado de RK-DOPRI
double	old_ac_OBp, old_ac_OBa, old_ac_OCa, old_ac_fvas;	//vari�veis aux�liares do alg de soma de Kahan
double	KSc_OBp, KSc_OBa, KSc_OCa, KSc_fvas;				//Corretores do algoritmo de soma de Kahan

//Popula��es celulares//
//		Nota��o		Unidade		Descri��o
double	C_OBu;	 	//	pM		Popula��o de precursores de osteoblastos
double	C_OBp_i; 	//	pM		Popula��o de pr�-osteoblastos da i-esima iteracao
double	C_OBp_n_i; 	//	pM		Popula��o normalizada de pr�-osteoblastos da i-esima iteracao
double	C_OBp_ini; 	//	pM		Popula��o inicial de pr�-osteoblastos do modelo de remodelagem
double	C_OBa_i; 	//	pM		Popula��o de osteoblastos ativos da i-esima iteracao
double	C_OBa_n_i; 	//	pM		Popula��o normalizada de osteoblastos ativos da i-esima iteracao
double	C_OBa_ini; 	//	pM		Popula��o inicial de osteoblastos ativos
double	C_OCp;	 	//	pM		Popula��o de precursores de osteoclastos
double	C_OCa_i;	//	pM		Popula��o de osteoclastos ativos da i-esima iteracao
double	C_OCa_n_i; 	//	pM		Popula��o normalizada de osteoclastos ativos da i-esima iteracao
double	C_OCa_ini;	//	pM		Popula��o inicial de osteoclastos ativos

//Par�metros do modelo de fra��o de volume e densidade mineral//
//		Nota��o		Unidade		Descri��o
double	df_vas;
double	f_bm_i; 	//	-		Fra��o de matriz �ssea da i-esima iteracao
//double	f_vas_ini; 	//	-		Fra��o de matriz �ssea inicial
double	f_vas_i; 	//	-		Fra��o de canais vasculares da i-esima iteracao
double	f_exlacbm_i;	//	-		Fra��o extralacunar da matriz ossea da i-esima iteracao
double	f_lac;		//	-		Fra��o volum�trica de lacunas na matriz �ssea
double	por_i;		//	-		Porosidade da i-esima itera��o
double	por_ini;	//	-		Porosidade inicial
double	Kform	; 	//	pM-1.dia-1	Taxa relativa de deposi��o de osso
double	Kres	; 	//	pM-1.dia-1	Taxa relativa de absor��o de osso
double	bmd_ref;	//	mg.HA/ml	Densidade mineral ossea obtida diretamente da porosidade
double	bmd_ref_max;	//	mg.HA/ml	Densidade mineral ossea obtida diretamente da porosidade
double	bmd_i;		//	mg.HA/ml	Densidade mineral ossea da i-esima iteracao
double	bmd_ini;	//	mg.HA/ml	Densidade mineral ossea inicial
double	bmd_delta_nrm_i;//	%	Diferenca da BMD atual para a inicial em %
double	tmin;		//	dia		Intervalo de tempo de mineraliza��o completa do tecido novo
double	coef_reta_min;//		Coeficiente da reta de mineraliza��o
double	Rv;			//	-		Raz�o entre o volume das c�lulas (OBa+OCa) e o volume vascular

//Constantes do modelo de remodelagem//
//		Nota��o		Unidade		Descri��o
double	DOBu_piv; 	//	dia-1	Taxa de diferencia��o m�xima de progenitores de osteoblastos
double	aPOBp	; 	//	-		Fra��o de prolifera��o de preosteoblastos
double	DOBu	; 	//	dia-1	Taxa de diferencia��o dos osteoblastos acometidos
double	DOBp	; 	//	dia-1	Taxa de diferencia��o das c�lulas precursoras dos osteoblastos
double	DOCp	; 	//	dia-1	Taxa de diferencia��o das c�lulas precursoras dos osteoclastos
double	POBp	; 	//	dia-1	Taxa de prolifera��o das c�lulas precursoras dos osteoblastos
double	AOBa	; 	//	dia-1	Taxa de apoptose de osteoblastos ativos
double	AOCa	; 	//	dia-1	Taxa de apoptose dos osteoclastos
double	Htb_act_obu;//	pM		Coeficiente de ativa��o relacionado a liga��o de TGF-? nos osteoblastos acometidos e osteoclastos
double	Htb_rep_obp;//	pM		Coeficiente de repress�o relacionado a liga��o de TGF-? nos precursores dos osteoblastos
double	Htb_act_oca;//	pM		Coeficiente de ativa��o relacionado a liga��o de TGF-? nos osteoblastos acometidos e osteoclastos
double	Hrkl_act_ocp;//	pM		Coeficiente de equil�brio para liga��o (dissocia��o) do RANKL no RANK
double	alfa	; 	//	pM.%-1	Conteudo de TGF-? armazenado na matriz �ssea
double	Tmv_TGFb;	//	min		Tempo de meia vida da mol�cula de TGF-b
double	D_TGFb	; 	//	dia-1	Taxa de degrada��o de TGF-b
double	Beta_PTH;	//	pM.dia-1	Taxa de produ��o de PTH sist�mico
double	Tmv_PTH ;	//	min		Tempo de meia vida da mol�cula de PTH
double	D_PTH	; 	//	dia-1	Taxa de degrada��o de PTH
double	Hpth_act_ob;//	pM		Coeficiente de ativa��o relacionado a liga��o de PTH nos precursores dos osteoblastos e osteoblastos ativos regulando o RANKL	Pivonka 2010	Pivonka 2008
double	Hpth_rep_ob;//	pM		Coeficiente de repress�o relacionado a liga��o de PTH nos precursores dos osteoblastos e osteoblastos ativos regulando o OPG	Pivonka 2010	Pivonka 2008
double	p_opg_ob;	//	dia-1	Taxa de produ��o de OPG (Por precursores dos osteoblastos)	Pivonka 2010	Estimado
//double	p_opg_obp; //	dia-1	Taxa de produ��o de OPG (Por precursores dos osteoblastos)	Pivonka 2010	Estimado
//double	p_opg_oba; //	dia-1	Constante de proporcionalidade que quantifica a produ��o de OPG por osteoblastos ativos	Pivonka 2010	Estimado
double	D_opg	; 	//	dia-1	Taxa de degrada��o de OPG	Pivonka 2010	Ref 83
double	C_OPG_max;	//	pM		Concentra��o m�xima poss�vel de OPG	Pivonka 2010	Ref 69 (nota 2)
double	R_OPG_max;	//	pM		Raz�o entre Concentra��o m�xima poss�vel de OPG	e concentra��o de regime permanente
double	Beta_rkl_max; //pM.dia-1	Taxa de produ��o de RANKL	Pivonka 2010	Estimado
double	Tmv_RKL ;	//	min		Tempo de meia vida da mol�cula de RANKL
double	D_rankl	; 	//	dia-1	Taxa de degrada��o de RANKL	Pivonka 2010	Ref 83
double	C_RANK	; 	//	pM		Concentra��o fixa de RANK	Pivonka 2010	Pivonka 2008
double	N_RKL_OB;	//	-		N�mero m�ximo de RANKL em cada superf�cie de c�lula Pivonka 2010	Pivonka 2008
//double	N_RKL_OBp;	//	-	N�mero m�ximo de RANKL em cada superf�cie de c�lula (Precursor dos osteoblastoss)	Pivonka 2010	Pivonka 2008
//double	N_RKL_OBa;	//	-	N�mero m�ximo de receptores de RANKL nos osteoblastos ativos	Pivonka 2010	Pivonka 2009
double	N_PTHR_OB;	//	-		N�mero de receptores de PTH na superf�cie de cada OBa ou OBp
double	Ka_PTH_R;	//	pM-1	Constante de associa��o PTH-PTH1R
double	Ka_RL_OPG;	//	pM-1	Constante de associa��o RANKL-OPG	Pivonka 2010	Ref 84
double	Ka_RL_R	;	//	pM-1	Constante de associa��o RANKL-RANK	Pivonka 2010	Ref 84

//Par�metros do modelo de remodelagem calculados//
//		Nota��o			Unidade		Descri��o
double	Pi_mech_act_obp;//-			Funcao de ativacao da proliferecao de pre-osteoblasto por estimulo mecanico
double	Beta_rkl_mech;	//pM.dia^-1	Taxa de producao de RANKL produzido a partir do estimulo mec�nico
double	C_TGFb_0;			//pM
double	TGFb;			//pM		Concentracao de TGF-b
double	Pi_Tb_act_Obu;	//-			Funcao de ativacao da diferenciacao de OBu por TGF-b
double	Pi_Tb_rep_Obp;	//-			Funcao de repressao da diferenciacao de OBp por TGF-b
double	Pi_Tb_act_oca;	//-			Funcao de ativacao da apoptose de osteoclastos ativos por TGF-b
double	Pi_Tb_act_Obu_0;//%			Valor inicial da Funcao de ativacao da diferenciacao de OBu por TGF-b
double	Pi_Tb_rep_Obp_0;//%			Valor inicial da Funcao de repressao da diferenciacao de OBp por TGF-b
double	Pi_Tb_act_Oca_0;//%			Valor inicial da Funcao de ativacao da apoptose de osteoclastos ativos por TGF-b
double	C_PTH;			//pM		Concentracao de PTH sist�mico livres
double	C_PTH_PTR;		//pM		Concentracao do composto PTH-PTH1R
double	C_PTHR;			//pM		Concentracao total de PTH1R da superf�cie das celulas
double	Pi_pth_act_ob;	//-			Funcao de ativacao da expressao maxima de RANKL nos osteoblastos e pr�-osteoblastos por PTH
double	Pi_pth_rep_ob;	//-			Funcao de repressao da expressao maxima de OPG nos osteoblastos e pr�-osteoblastos por PTH
double	Pi_pth_act_ob_0;//%			Valor inicial da Funcao de ativacao da expressao maxima de RANKL nos osteoblastos e pr�-osteoblastos por PTH
//double	Pi_pth_rep_ob_0;//%			Valor inicial da Funcao de repressao da expressao maxima de OPG nos osteoblastos e pr�-osteoblastos por PTH
double	Beta_opg_max;	//pM.dia^-1	Taxa de produ��o maxima de OPG
double	C_OPG_0;
double	OPG;			//pM		Concentracao de OPG
double	OPGtot;			//-			Fator de proporcao entre OPG livre e OPG total
double	RKLmax;			//pM		Concentracao maxima de RANKL considerando OBp e OBa
double	RKLtot;			//-			Fator de proporcao entre RANKL livre e RAANKL total
double	C_RKL_0;
double	C_RKL;			//pM		Concentracao de RANKL
double	C_RKL_RK;		//pM		Concentracao do complexo RANK-RANKL
double	Pi_rkl_rk_act_ocp;//-		Funcao de ativacao da diferenciacao de OCp por RANKL
double	Pi_rkl_rk_act_ocp_0;//-		Valor inicial da Funcao de ativacao da diferenciacao de OCp por RANKL
double	dC_OBp;			//pM		Taxa de variacao da concentracao de OBp no tempo
double	dC_OBa;			//pM		Taxa de variacao da concentracao de OBa no tempo
double	dC_OCa;			//pM		Taxa de variacao da concentracao de OCa no tempo

//Par�metros da biomec�nica//
//		Nota��o		Unidade		Descri��o
double	Pi_mech_ref;//	-		Valor m�nimo de referencia da Funcao de ativacao da proliferecao de pre-osteoblasto por estimulo mecanico
double	Pi_mech_max;//	-		Valor m�ximo da Funcao de ativacao da proliferecao de pre-osteoblasto por estimulo mecanico
double	lambda1	;	//	-		Coeficiente de inclinacao da reta de subida na funcao de regulacao da proliferacao de OBp por estimulo mec�nico
double	lambda2	;	//	-		Coeficiente de inclinacao da reta de descida na funcao de regulacao da proliferacao de OBp por estimulo mec�nico
double	kappa1	;	//pM.dia-1	Coeficiente de inclinacao da reta de descida na funcao de producao de RANKL por estimulo mec�nico
double	kappa2	;	//pM.dia-1	Coeficiente de inclinacao da reta de subida na funcao de producao de RANKL por estimulo mec�nico
double	wcort	;	//Pa		Est�mulo mec�nico de energia de deformacao no cortical
double	w		;	//Pa		Est�mulo mec�nico de energia de deformacao na matrix �ssea
double	w_r1	;	//Pa		Estimulo mecanico inferior entre regiao de reabsorcao e zona morta
double	w_r2	; 	//Pa		Estimulo mecanico superior entre regiao de zona morta e reabsorcao
double	w_f1	; 	//Pa		Estimulo mecanico de inicio da subida ao patamar de formacao
double	w_f2	;	//Pa		Estimulo mecanico de inicio do patamar de formacao
double	w_f3	;	//Pa		Estimulo mecanico de fim do patamar de formacao e inicio da reta de descida
double	w_f4	;	//Pa		Estimulo mecanico do fim da descida do patamar de formacao
double	c_vas[21]; 	//GPa		Matriz de rigidez da 'inclus�o' vascular
double	c_bm[21]; 	//GPa		Matriz de rigidez da matriz �ssea
double	c_cort[21]; //GPa		Matriz de rigidez do osso cortical
double	epsilon[6]; //-			Vetor de deforma��o do tecido - [1] Dire��o Radial; [2] Dire��o Angular; [3] Dire��o Longitudinal
double	sigma[6];	//Pa		Vetor de tensao do tecido

//Perturbacoes//
//		Nota��o		Unidade		Descri��o
double	DiaP_ini;	//	dia		Dia inicial de perturbacao
double	DiaP_fim; 	//	dia		Dia final de perturbacao
double	deltat_pert;//	dia		Per�odo de tempo da rampa de perturba��o
double	deltat_rest;//	dia		Per�odo de tempo da rampa de restaura��o
double	freq_pert;	//	pM		Intervalo de tempo entre aplica��es consecutivas de pertuba��o.
double	repeticoes;	//	-		Numero de repeticoes no exercicio ou perturbacao
double	con_rept;	//	-		Contador do numero de repeticoes
double	P_TGFb	; 	//	pM		Variavel referente a injecao externa de TGFb
double	P_PTH	; 	//	pM		Variavel referente a injecao externa de PTH
double	P_OPG	; 	//	pM		Variavel referente a injecao externa de OPG
double	P_RANKL; 	//	pM		Variavel referente a injecao externa de RANKL

//Arquivos de escrita//
double	vet_esc[60];//Vetor para armazenar valores de saida do modelo
double	vet_info[60];//Vetor para armazenar valores de saida dos parametros do metodo numerico itlin
FILE	*ofp_pop;	//Arquivo dos valores de popula��o
FILE	*ofp_pim;	//Arquivo dos vaLores dos Par�mteros aquisitados de Imagem Medica
FILE	*ofp_mtr;	//Arquivo dos valores da matriz de rigidez do osso
FILE	*ofp_rmd;	//Arquivo dos valores do parametros de remodelagem
FILE	*ofp_ork;	//Arquivo com sumario dos parametros do metodo itlin de OPG e RANKL
FILE	*ofp_mrk;	//Arquivo dos valores do metodo de runge-kutta

//Arquivos de escrita para pos processamento e variaveis relativas as caracteristicas da aquisicao no tempo//
FILE	*ofp_pos_tot; 	//Arquivo com dados a serem pos processados, aquisitados em tempo total
FILE	*ofp_pos_par; 	//Arquivo com dados a serem pos processados, aquisitados em um intervalo de tempo (aquisicao parcial)
double	Dia_ini_aqpar;	//Variavel referente ao Dia inicial da aquisical parcial
double	int_dias_aq;	//Variavel referente ao intervalo de tempo em dias da aquisicao parcial
FILE	*ofp_pos_lfaq;	//Arquivo com dados a serem pos processados, aquisitados em uma frequencia menor
double	dia_lfaq_ref;	//Variavel referente ao dia de referencia para aquisicao em uma frequencia menor


#endif /* _VAR_COMMON */
