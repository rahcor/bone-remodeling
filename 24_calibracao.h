// Rotina que calcula a integral de f_bm pelo metodo de Euler 
#ifndef _CALIBRACAO_H
#include "00_var_common.h"
#include "22_atribuicao.h"
#include "70_remodelagem.h"
#include <math.h>
#define	_CALIBRACAO_H

double	convTmv_to_D (double Tmv);	//Fun��o que converte o valor de meia vida da mol�cula para taxa de degrada��o ao dia

void calibracao (void)
{
	//C�lculos das constantes dependentes
//			printf ("\n>Calculos das constantes dependentes.\n");
	if (Kipt==iptKform)									//Se Kform foi dado de input
				Kres = Kform * (C_OBa_ini/C_OCa_ini);
	else if (Kipt==iptKres)								//Se Kres foi dado de input
				Kform = Kres * (C_OCa_ini/C_OBa_ini);
	D_TGFb = convTmv_to_D (Tmv_TGFb);
	D_PTH = convTmv_to_D (Tmv_PTH);
	D_rankl = convTmv_to_D (Tmv_RKL);
	
	Htb_act_obu = C_TGFb_0 * (1.0-Pi_Tb_act_Obu_0)/Pi_Tb_act_Obu_0;
	Htb_rep_obp = C_TGFb_0 * Pi_Tb_rep_Obp_0/(1.0-Pi_Tb_rep_Obp_0);
	Htb_act_oca = C_TGFb_0 * (1.0-Pi_Tb_act_Oca_0)/Pi_Tb_act_Oca_0;
	C_RKL_RK = Ka_RL_R * C_RKL_0 * C_RANK;
	Hrkl_act_ocp = C_RKL_RK * (1.0-Pi_rkl_rk_act_ocp_0)/Pi_rkl_rk_act_ocp_0;
	
	alfa = C_TGFb_0 * D_TGFb / (Kres * C_OCa_ini);
	TGFb = (alfa * Kres * C_OCa_ini + P_TGFb) / D_TGFb;
	Pi_Tb_act_Obu = TGFb / (Htb_act_obu + TGFb);
	Pi_Tb_rep_Obp = Htb_rep_obp / (Htb_rep_obp + TGFb);
	Pi_Tb_act_oca = TGFb / (Htb_act_oca + TGFb);
	
	C_PTHR = N_PTHR_OB * (C_OBp_ini + C_OBa_ini);
	C_PTH = (Beta_PTH + P_PTH) / (D_PTH * (1 + Ka_PTH_R * C_PTHR));
	C_PTH_PTR = Ka_PTH_R * C_PTH * C_PTHR;
	Pi_pth_rep_ob = Hpth_rep_ob / (Hpth_rep_ob + C_PTH_PTR);
	Hpth_act_ob = C_PTH_PTR * (1.0-Pi_pth_act_ob_0)/Pi_pth_act_ob_0;
	Pi_pth_act_ob = C_PTH_PTR / (Hpth_act_ob + C_PTH_PTR);
	
	C_OPG_max = R_OPG_max * C_OPG_0;
	Beta_opg_max = p_opg_ob * (C_OBp_ini + C_OBa_ini) * Pi_pth_rep_ob;
	D_opg = ( (R_OPG_max-(1.0+Ka_RL_OPG*C_RKL_0)) / (R_OPG_max*(1.0+Ka_RL_OPG*C_RKL_0)) )*(Beta_opg_max/C_OPG_0);
	
	RKLmax = N_RKL_OB * (C_OBp_ini + C_OBa_ini) * Pi_pth_act_ob;
	RKLtot = (1.0 + Ka_RL_OPG*C_OPG_0 + Ka_RL_R*C_RANK);
	Beta_rkl_max = D_rankl / (1.0/(RKLtot*C_RKL_0)-1.0/RKLmax);
	
	lambda1 = (Pi_mech_max-Pi_mech_ref)/((w_f2/w_f1)-1.0);
	lambda2 = -(Pi_mech_max+Pi_mech_ref)/((w_f4/w_f3)-1.0);
	
	/////	
	OPG = 1.1*C_OPG_0;				//Chute inicial de OPG
	C_RKL = 1.1*C_RKL_0;			//Chute inicial de RKL
	itlin_OPG_RKL ();						//Solucao de OPG e C_RKL atravez do metodo iterativo linear
	if (flagesc!=0) fprintf (ofp_ork, "calibracao \t %.5E \t %.5E \t %.5E \t %.5E \t %.5E\n", vet_info[0], vet_info[1], vet_info[2], vet_info[3], vet_info[4]);	//Escreve no arquivo as infos do m�todo itlin de OPG-RKL
	
	C_RKL_RK = Ka_RL_R * C_RKL * C_RANK;
	Pi_rkl_rk_act_ocp = C_RKL_RK / (Hrkl_act_ocp + C_RKL_RK);	
	/////
	
	DOBp = (AOBa*C_OBa_ini)/(C_OBp_ini*Pi_Tb_rep_Obp);						//Calibra��o de DOBp
	DOBu_piv = (DOBp*C_OBp_ini*Pi_Tb_rep_Obp)/(C_OBu*Pi_Tb_act_Obu);		//Calibra��o de DOBu_piv
	DOBu = (1.0-aPOBp) * DOBu_piv;
	POBp = 	(DOBu_piv*C_OBu*Pi_Tb_act_Obu*aPOBp)/(C_OBp_ini*Pi_mech_ref);
	DOCp = (AOCa*C_OCa_ini*Pi_Tb_act_oca)/(C_OCp*Pi_rkl_rk_act_ocp);		//Calibra��o de DOCp
}

double	convTmv_to_D (double Tmv)	//Fun��o que converte o valor de meia vida da mol�cula para taxa de degrada��o ao dia
{
	return (log(2.0)/(Tmv/1440.0));			//O fator '1440' corresponde ao n�mero de minutos em um dia. A fun��o log corresponde ao logaritmo natural.
}

#endif

