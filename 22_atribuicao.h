// Rotina de atribui��o de valores para as vari�veis globais do programa.
#ifndef _ATRIBUICAO
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "00_var_common.h"
#define	_ATRIBUICAO

int	rtrn_cte_id	(char *strg, struct tipo_cte *ctes);	//Funcao que retorna o id da constante assim que encontra um nome igual ao recebido, buscando em todo vetor ctes

void atribuicao_config (struct tipo_cte *ctes)			//Atribuicao dos valores das configs. do vetor 'vet_ctes' para as variaveis globais. (22_atribuicao.h)
{
//	printf ("\n>Atribuicao dos valores do vetor para as variaveis globais de mesmo nome.\n");
	//Chamadas da fun��o rtrn_cte_id para cada constante
	h			 = ctes[rtrn_cte_id("h",ctes)].valor;
	h_min		 = ctes[rtrn_cte_id("h_min",ctes)].valor;
	h_max		 = ctes[rtrn_cte_id("h_max",ctes)].valor;
	errotgt		 = ctes[rtrn_cte_id("errotgt",ctes)].valor;
	vol_tgt		 = ctes[rtrn_cte_id("vol_tgt",ctes)].valor;
	it_max		 = ctes[rtrn_cte_id("it_max",ctes)].valor;
	tempo_final	 = ctes[rtrn_cte_id("tempo_final",ctes)].valor;
	DiaP_ini	 = ctes[rtrn_cte_id("DiaP_ini",ctes)].valor;
	DiaP_fim	 = ctes[rtrn_cte_id("DiaP_fim",ctes)].valor;
	deltat_pert	 = ctes[rtrn_cte_id("deltat_pert",ctes)].valor;
	deltat_rest	 = ctes[rtrn_cte_id("deltat_rest",ctes)].valor;
	freq_pert	 = ctes[rtrn_cte_id("freq_pert",ctes)].valor;
	repeticoes	 = ctes[rtrn_cte_id("repeticoes",ctes)].valor;
	por_ini		 = ctes[rtrn_cte_id("por_ini",ctes)].valor;		//f_vas_ini	 = ctes[rtrn_cte_id("f_vas_ini",ctes)].valor;
	f_lac		 = ctes[rtrn_cte_id("f_lac",ctes)].valor;
	C_OBa_ini	 = ctes[rtrn_cte_id("C_OBa_ini",ctes)].valor;
	C_OBp_ini	 = ctes[rtrn_cte_id("C_OBp_ini",ctes)].valor;
	C_OCa_ini	 = ctes[rtrn_cte_id("C_OCa_ini",ctes)].valor;
	tmin		 = ctes[rtrn_cte_id("tmin",ctes)].valor;
	
	if ( DiaP_ini > tempo_final ) {
		printf ("\n\nO tempo final especificado e' menor que o dia inicial da perturbacao\n\n");
		status_ctes=RESTAURADO;
	}
	//Verificacao de validade do numero de repeticoes vs deltat_pert vs freq_pert
	if ( (repeticoes != 0) && (freq_pert != 0) ) {
		if ( repeticoes*(deltat_pert+deltat_rest) > freq_pert ) {
			printf ("\n\nO intervalo entre perturbacoes eh menor que o tempo de aplicacao da perturbacao multiplicado pelo numero de repeticoes!!");
			printf ("\nPor gentileza, aumente freq_pert ou diminua deltat_pert ou diminua o numero de repeticoes.\n\n");
			exit(0);
		}
	}
	else 	printf ("\n\nModo de perturbacao continua selecionado.\n\n");

//	printf("\n>>>Fim do procedimento de atribuicao.\n\n");
}

void atribuicao_ctes (struct tipo_cte *ctes)
{
	int	con, aux; 				//con=Contador

//	printf ("\n>Atribuicao dos valores do vetor para as variaveis globais de mesmo nome.\n");
	//Chamadas da fun��o rtrn_cte_id para cada constante	
	{
		AOBa		 = ctes[rtrn_cte_id("AOBa", ctes)].valor;
		AOCa		 = ctes[rtrn_cte_id("AOCa", ctes)].valor;
		aPOBp		 = ctes[rtrn_cte_id("aPOBp",ctes)].valor;
		C_OBu		 = ctes[rtrn_cte_id("C_OBu",ctes)].valor;
		C_OCp		 = ctes[rtrn_cte_id("C_OCp",ctes)].valor;
		C_OPG_0		 = ctes[rtrn_cte_id("C_OPG_0", ctes)].valor;
//		C_OPG_max	 = ctes[rtrn_cte_id("C_OPG_max", ctes)].valor;
		R_OPG_max	 = ctes[rtrn_cte_id("R_OPG_max", ctes)].valor;
		C_RANK		 = ctes[rtrn_cte_id("C_RANK", ctes)].valor;
		C_RKL_0		 = ctes[rtrn_cte_id("C_RKL_0", ctes)].valor;
		C_TGFb_0	 = ctes[rtrn_cte_id("C_TGFb_0", ctes)].valor;
//		DOBp		 = ctes[rtrn_cte_id("DOBp", ctes)].valor;
//		DOBu_piv	 = ctes[rtrn_cte_id("DOBu_piv", ctes)].valor;
//		DOBu		 = ctes[rtrn_cte_id("DOBu", ctes)].valor;
//		DOCp		 = ctes[rtrn_cte_id("DOCp", ctes)].valor;
//		D_opg	 	 = ctes[rtrn_cte_id("D_opg", ctes)].valor;
		Tmv_PTH		 = ctes[rtrn_cte_id("Tmv_PTH", ctes)].valor;
//		D_PTH		 = ctes[rtrn_cte_id("D_PTH", ctes)].valor;
		Tmv_RKL		 = ctes[rtrn_cte_id("Tmv_RKL", ctes)].valor;
//		D_rankl		 = ctes[rtrn_cte_id("D_rankl", ctes)].valor;
		Tmv_TGFb	 = ctes[rtrn_cte_id("Tmv_TGFb", ctes)].valor;
//		D_TGFb		 = ctes[rtrn_cte_id("D_TGFb", ctes)].valor;
//		Hpth_act_ob	 = ctes[rtrn_cte_id("Hpth_act_ob", ctes)].valor;
//		Hpth_rep_ob	 = ctes[rtrn_cte_id("Hpth_rep_ob", ctes)].valor;
//		Hrkl_act_ocp	 = ctes[rtrn_cte_id("Hrkl_act_ocp", ctes)].valor;
//		Htb_act_obu	 = ctes[rtrn_cte_id("Htb_act_obu", ctes)].valor;
//		Htb_act_oca	 = ctes[rtrn_cte_id("Htb_act_oca", ctes)].valor;
//		Htb_rep_obp	 = ctes[rtrn_cte_id("Htb_rep_obp", ctes)].valor;
		Pi_pth_act_ob_0	 = ctes[rtrn_cte_id("Pi_pth_act_ob_0", ctes)].valor;
		Hpth_rep_ob		 = ctes[rtrn_cte_id("Hpth_rep_ob", ctes)].valor;
		Pi_rkl_rk_act_ocp_0 = ctes[rtrn_cte_id("Pi_rkl_rk_act_ocp_0", ctes)].valor;
		Pi_Tb_act_Obu_0	 = ctes[rtrn_cte_id("Pi_Tb_act_Obu_0", ctes)].valor;
		Pi_Tb_act_Oca_0	 = ctes[rtrn_cte_id("Pi_Tb_act_Oca_0", ctes)].valor;
		Pi_Tb_rep_Obp_0	 = ctes[rtrn_cte_id("Pi_Tb_rep_Obp_0", ctes)].valor;
		Ka_RL_OPG	 = ctes[rtrn_cte_id("Ka_RL_OPG", ctes)].valor;
		Ka_RL_R		 = ctes[rtrn_cte_id("Ka_RL_R", ctes)].valor;
		Ka_PTH_R		 = ctes[rtrn_cte_id("Ka_PTH_R", ctes)].valor;
		if (Kipt==iptKform)									//Se Kform foi dado de input
				Kform = ctes[rtrn_cte_id("Kform",ctes)].valor;
		else if (Kipt==iptKres)								//Se Kres foi dado de input
				Kres = ctes[rtrn_cte_id("Kres", ctes)].valor;
		N_PTHR_OB	 = ctes[rtrn_cte_id("N_PTHR_OB", ctes)].valor;
		N_RKL_OB	 = ctes[rtrn_cte_id("N_RKL_OB", ctes)].valor;
//		N_RKL_OBp	 = ctes[rtrn_cte_id("N_RKL_OBp", ctes)].valor;
//		N_RKL_OBa	 = ctes[rtrn_cte_id("N_RKL_OBa", ctes)].valor;
		p_opg_ob	 = ctes[rtrn_cte_id("p_opg_ob", ctes)].valor;
//		p_opg_obp = ctes[rtrn_cte_id("p_opg_obp", ctes)].valor;
//		p_opg_oba = ctes[rtrn_cte_id("p_opg_oba", ctes)].valor;			
//		alfa		 = ctes[rtrn_cte_id("alfa", ctes)].valor;
		Beta_PTH	 = ctes[rtrn_cte_id("Beta_PTH", ctes)].valor;
//		Beta_rkl_max = ctes[rtrn_cte_id("Beta_rkl_max", ctes)].valor;
		Pi_mech_ref	 = ctes[rtrn_cte_id("Pi_mech_ref", ctes)].valor;
		Pi_mech_max	 = ctes[rtrn_cte_id("Pi_mech_max", ctes)].valor;
		kappa1		 = ctes[rtrn_cte_id("kappa1", ctes)].valor;
		kappa2		 = ctes[rtrn_cte_id("kappa2", ctes)].valor;
		w_r1		 = ctes[rtrn_cte_id("w_r1", ctes)].valor;
		w_f1		 = ctes[rtrn_cte_id("w_f1", ctes)].valor;
		w_f2		 = ctes[rtrn_cte_id("w_f2", ctes)].valor;
		w_f3		 = ctes[rtrn_cte_id("w_f3", ctes)].valor;
		w_f4		 = ctes[rtrn_cte_id("w_f4", ctes)].valor;
		w_r2		 = ctes[rtrn_cte_id("w_r2", ctes)].valor;
		P_TGFb		 = ctes[rtrn_cte_id("P_TGFb", ctes)].valor;	
		P_PTH		 = ctes[rtrn_cte_id("P_PTH", ctes)].valor;		
		P_OPG		 = ctes[rtrn_cte_id("P_OPG", ctes)].valor;		
		P_RANKL		 = ctes[rtrn_cte_id("P_RANKL", ctes)].valor;
		aux = rtrn_cte_id("c_bm_11", ctes);				//Busca a posicao do primeiro termo da matriz no vetor de ctes. Obs: n�o pode mudar ordem do vetor no input.txt!!!
		for	(con=0; con<21 ; con++)						//Para todo comprimento do vetor
			c_bm[con]=ctes[aux+con].valor*1.0E9;		//Atribui os valores na sequencia pr�viamente definida. O termo '*1.0E9' Realiza a convers�o de unidades de GPa para Pa
		aux = rtrn_cte_id("c_vas_11", ctes);			//Busca a posicao do primeiro termo da matriz no vetor de ctes. Obs: n�o pode mudar ordem do vetor no input.txt!!!
		for	(con=0; con<21 ; con++)						//Para todo comprimento do vetor
			c_vas[con]=ctes[aux+con].valor*1.0E9;		//Atribui os valores na sequencia pr�viamente definida. O termo '*1.0E9' Realiza a convers�o de unidades de GPa para Pa
		if ( Tipt == ipteps ) {			// Obs: epsilon � utilizado no dia da perturba��o. Fora dela s�o assumidas as regi�es da homeostase mec�nica.
			epsilon[0]=ctes[rtrn_cte_id("eps_1", ctes)].valor;
			epsilon[1]=ctes[rtrn_cte_id("eps_2", ctes)].valor;
			epsilon[2]=ctes[rtrn_cte_id("eps_3", ctes)].valor;
			epsilon[3]=ctes[rtrn_cte_id("eps_4", ctes)].valor;
			epsilon[4]=ctes[rtrn_cte_id("eps_5", ctes)].valor;
			epsilon[5]=ctes[rtrn_cte_id("eps_6", ctes)].valor;
		} else if ( Tipt == iptsig ) {
			sigma[0]=ctes[rtrn_cte_id("sig_1", ctes)].valor;
			sigma[1]=ctes[rtrn_cte_id("sig_2", ctes)].valor;
			sigma[2]=ctes[rtrn_cte_id("sig_3", ctes)].valor;
			sigma[3]=ctes[rtrn_cte_id("sig_4", ctes)].valor;
			sigma[4]=ctes[rtrn_cte_id("sig_5", ctes)].valor;
			sigma[5]=ctes[rtrn_cte_id("sig_6", ctes)].valor;
		}											

//		printf ("Amostra:\nCRANK= %.3E \t kappa2= %.3E \t c_bm_22= %.3E \t eps_3= %.3E.\n", C_RANK, kappa2, c_bm[6], epsilon[2]); //Debug: imprime alguns valores para verifica��o
	}
	
//	printf("\n>>>Fim do procedimento de atribuicao.\n\n");
}

void atribuicaoinv_calc (struct tipo_cte *ctes)			//Realiza a atribui��o das constantes calculadas para o vetor
{
	ctes[rtrn_cte_id("DOBp", ctes)].valor = DOBp;
	ctes[rtrn_cte_id("DOBu", ctes)].valor = DOBu;
	ctes[rtrn_cte_id("DOCp", ctes)].valor = DOCp;
	
	ctes[rtrn_cte_id("D_TGFb", ctes)].valor 	= D_TGFb;
	ctes[rtrn_cte_id("D_PTH", ctes)].valor 		= D_PTH; 
	ctes[rtrn_cte_id("D_rankl", ctes)].valor 	= D_rankl;
	ctes[rtrn_cte_id("D_opg", ctes)].valor 		= D_opg;
	
	ctes[rtrn_cte_id("Htb_act_obu", ctes)].valor	= Htb_act_obu;
	ctes[rtrn_cte_id("Htb_rep_obp", ctes)].valor 	= Htb_rep_obp;
	ctes[rtrn_cte_id("Htb_act_oca", ctes)].valor 	= Htb_act_oca;
	ctes[rtrn_cte_id("Hrkl_act_ocp", ctes)].valor	= Hrkl_act_ocp;
	ctes[rtrn_cte_id("Hpth_act_ob", ctes)].valor	= Hpth_act_ob;
	
	ctes[rtrn_cte_id("C_OPG_max", ctes)].valor		= C_OPG_max;
	ctes[rtrn_cte_id("alfa", ctes)].valor 			= alfa;
	ctes[rtrn_cte_id("Beta_rkl_max", ctes)].valor 	= Beta_rkl_max;
	
	printf ("Estimulo mec e Constantes calculadas:\n");
	int con=0;
	for (con=nlin-25; con<nlin ; con++)	//La�o para imprimir na tela as linhas do vetor referentes as ctes calculadas
		printf ("Linha inserida %2d: %.3E \t %s\n", con+1, vet_ctes[con].valor, vet_ctes[con].nome);
}

void atribuicao_calc (struct tipo_cte *ctes)			//Realiza a atribui��o das constantes calculadas para o vetor
{
	DOBp 			= ctes[rtrn_cte_id("DOBp", ctes)].valor;
	DOBu 			= ctes[rtrn_cte_id("DOBu", ctes)].valor;
	DOCp 			= ctes[rtrn_cte_id("DOCp", ctes)].valor;
	
	D_TGFb 			= ctes[rtrn_cte_id("D_TGFb", ctes)].valor;
	D_PTH 			= ctes[rtrn_cte_id("D_PTH", ctes)].valor; 
	D_rankl 		= ctes[rtrn_cte_id("D_rankl", ctes)].valor;
	D_opg 			= ctes[rtrn_cte_id("D_opg", ctes)].valor;
	
	Htb_act_obu 	= ctes[rtrn_cte_id("Htb_act_obu", ctes)].valor;
	Htb_rep_obp 	= ctes[rtrn_cte_id("Htb_rep_obp", ctes)].valor;
	Htb_act_oca 	= ctes[rtrn_cte_id("Htb_act_oca", ctes)].valor;
	Hrkl_act_ocp	= ctes[rtrn_cte_id("Hrkl_act_ocp", ctes)].valor;
	Hpth_act_ob 	= ctes[rtrn_cte_id("Hpth_act_ob", ctes)].valor;
	
	C_OPG_max 		= ctes[rtrn_cte_id("C_OPG_max", ctes)].valor;
	alfa 			= ctes[rtrn_cte_id("alfa", ctes)].valor;
	Beta_rkl_max 	= ctes[rtrn_cte_id("Beta_rkl_max", ctes)].valor;
}


void atribuicaoinv_calc_pert (struct tipo_cte *ctes, struct tipo_cte *ctes_pert)			//Realiza a atribui��o das constantes calculadas para o vetor
{
	if (ctes_pert[rtrn_cte_id("Tmv_TGFb", ctes_pert)].valor!=ctes[rtrn_cte_id("Tmv_TGFb", ctes)].valor)		ctes_pert[rtrn_cte_id("D_TGFb", ctes_pert)].valor = D_TGFb;
	if (ctes_pert[rtrn_cte_id("Tmv_PTH", ctes_pert)].valor!=ctes[rtrn_cte_id("Tmv_PTH", ctes)].valor)		ctes_pert[rtrn_cte_id("D_PTH", ctes_pert)].valor = D_PTH; 
	if (ctes_pert[rtrn_cte_id("Tmv_RKL", ctes_pert)].valor!=ctes[rtrn_cte_id("Tmv_RKL", ctes)].valor)		ctes_pert[rtrn_cte_id("D_rankl", ctes_pert)].valor = D_rankl;
	
	if (ctes_pert[rtrn_cte_id("R_OPG_max", ctes_pert)].valor!=ctes[rtrn_cte_id("R_OPG_max", ctes)].valor)	ctes_pert[rtrn_cte_id("C_OPG_max", ctes_pert)].valor = C_OPG_max;
}


//////////Fun��es
int	rtrn_cte_id	(char *strg, struct tipo_cte *ctes)		//Funcao que retorna a posicao da constante de nome 'strg' assim que encontra um nome igual no vetor 'ctes'
{															//Caso n�o encontre nome igual, exibe mensagem de erro e sai do programa.
	int	con=0;	//contador
	
	while ( strcmp(ctes[con].nome,strg)!=0 && con!=nlin )	//Enquanto as strings forem diferentes e n�o for o fim do vetor. strcmp:Se valor for 0 as strings s�o iguais.
		con++;												//Vai para o pr�ximo indice do vetor

	if ( strcmp(ctes[con].nome, strg)==0 )				//Se as strings forem iguais,
		return	(con);										//Retorna o indice do vetor 'ctes' que se encontra o par�metro cujo nome � 'strg'
	else {												//Se varrer todas as posi��es do vetor ctes recebido e n�o encontrar string igual,
		printf ("\n%s nao encontrado! Verifique o nome das constantes.\n\n", strg);	//retorna uma mensagem de erro e
		exit(0);														 	     	// sai do programa.
	}
}

#endif
