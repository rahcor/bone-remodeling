// Funcoes para realiza��o da perturbacao
#ifndef _PERTURBACAO_H
#include "00_var_common.h"
#include "22_atribuicao.h"
#include "24_calibracao.h"
#include <string.h>
#define	_PERTURBACAO_H

void monta_vet_pert (void)		//Montagem do vetor de constantes perturbadas
{
	int con;
	
	//C�pia do vet_ctes para o vet_ctes_pert e vet_ctes_pert_aux
	for (con=0; con<nlin; con++) {
		vet_ctes_pert[con].valor = vet_ctes[con].valor;
		strcpy (vet_ctes_pert[con].nome, vet_ctes[con].nome);
		vet_ctes_pert_aux[con].valor = vet_ctes[con].valor;
		strcpy (vet_ctes_pert_aux[con].nome, vet_ctes[con].nome);
	}
	
	//Substituicao dos valores de perturba��o lidos para o vet_ctes_pert. Obs: o valor das variaveis globais n�o serao substituidos agora!!
	for (con=4 ; con<nlin_pert-1 ;con++) {								//Para toda exten��o do vetor
		if ( strcmp(vet_ctes_pert_lidos[con].tipo,"valor")==0 )			//Se o tipo de perturbacao � de 'valor'
			vet_ctes_pert[ rtrn_cte_id(vet_ctes_pert_lidos[con].nome,vet_ctes_pert) ].valor = vet_ctes_pert_lidos[con].valor;	//O valor de perturbacao lido � atribuido. (funcao rtrn_cte_id em 22_atribuicao.h)
		else if ( strcmp(vet_ctes_pert_lidos[con].tipo,"fracao")==0 )	//Se o tipo de perturbacao � de 'fracao'
			vet_ctes_pert[ rtrn_cte_id(vet_ctes_pert_lidos[con].nome,vet_ctes_pert) ].valor = vet_ctes_pert_lidos[con].valor*vet_ctes[ rtrn_cte_id(vet_ctes_pert_lidos[con].nome,vet_ctes) ].valor; //O valor atribuido � a multiplicacao do valor lido pelo valor da constante
		else if ( strcmp(vet_ctes_pert_lidos[con].tipo,"desl")!=0 ){		//Se o tipo de perturbacao n�o for 'valor', 'fracao' ou 'desl'
			printf ("\nTipo de perturbacao nao reconhecido. Verifique o nome do Tipo.");	//� mostrada mensagem de tipo inv�lido de perturbacao
			exit(0);																		//	e termina o programa.
		}
	}
		
	printf ("\n\n");	

	for (con=0; con<nlin; con++) 
		printf ("vet_ctes_pert %2d: %.3E \t %s\n", con, vet_ctes_pert[con].valor, vet_ctes_pert[con].nome);	//Debug: Imprime todo o vetor vet_ctes_pert com os valores substituidos.

	printf ("\n");
}

void perturbacao (void)
{
	int con=0;
	
	//Realizacao das perturba��es e recuper��o das constantes
	if (status_ctes == N_PERTURBADO) {						//Se as constantes n�o foram perturbadas
		if (tempo_atual >= DiaP_ini)						//Verifica se iniciou o per�odo de perturba��o do sistema
			status_ctes = RAMPA_PERT;						//Altera a situacao da perturbacao para rampa de aplicacao da perturbacao.
	}
	if (status_ctes == RAMPA_PERT) {
		for (con=0 ; con<nlin ; con++)
			vet_ctes_pert_aux[con].valor = vet_ctes[con].valor + (tempo_atual-(DiaP_ini+((con_rept-1)*(deltat_pert+deltat_rest))))*(vet_ctes_pert[con].valor-vet_ctes[con].valor)/deltat_pert; //Equa��o da rampa de subida
		atribuicao_ctes (vet_ctes_pert_aux);					//Atribui valores de perturba��o
		calibracao ();											//Calculo das constantes dependentes
		atribuicaoinv_calc_pert (vet_ctes, vet_ctes_pert_aux);	//Atribui��o inversa para as constantes dependentes calculadas
		atribuicao_calc (vet_ctes_pert_aux);					//Atribui��o dos valores calculados
				
		if (tempo_atual >= DiaP_ini+con_rept*(deltat_pert+deltat_rest)-deltat_rest ) {			//Se terminou o per�odo da rampa
			if (freq_pert!=0) {								//Caso a perturba��o N�O seja cont�nua
				status_ctes = RAMPA_REST;		//se freq!=0, altera status para RAMPA_REST para restaura��o das constantes 
			}
			else if (freq_pert==0) {							//Caso a perturbacao seja cont�nua
				atribuicao_ctes (vet_ctes_pert);					//Atribui valores de perturba��o
				calibracao ();										//Calculo das constantes dependentes
				atribuicaoinv_calc_pert (vet_ctes, vet_ctes_pert);	//Atribui��o inversa para as constantes dependentes calculadas
				atribuicao_calc (vet_ctes_pert);					//Atribui��o dos valores calculados
				status_ctes = PERTURBADO;						//Altera a situacao das constantes para pertubado.
			}
		}
	}
	else if (status_ctes == PERTURBADO)	{						
		if (tempo_atual >= DiaP_fim)	status_ctes = RAMPA_REST;	//Se terminou o per�odo da perturba��o Altera a situacao das constantes para rampa de restaura��o das constantes.
	}
	else if (status_ctes == RAMPA_REST) {
		for (con=0 ; con<nlin ; con++)
			 vet_ctes_pert_aux[con].valor = vet_ctes_pert[con].valor - (tempo_atual-(DiaP_ini+con_rept*(deltat_pert+deltat_rest)-deltat_rest))*(vet_ctes_pert[con].valor-vet_ctes[con].valor)/deltat_rest; //Equa��o da rampa de descida
		atribuicao_ctes (vet_ctes_pert_aux);					//Atribui valores de perturba��o
		calibracao ();											//Calculo das constantes dependentes
		atribuicaoinv_calc_pert (vet_ctes, vet_ctes_pert_aux);	//Atribui��o inversa para as constantes dependentes calculadas
		atribuicao_calc (vet_ctes_pert_aux);					//Atribui��o dos valores calculados
				
		if ( freq_pert!=0 && tempo_atual >= DiaP_ini+con_rept*(deltat_pert+deltat_rest) ) { //Se terminou o per�odo da rampa no caso N�O cont�nuo
			atribuicao_ctes (vet_ctes);							//Em caso positivo, restaura o sistema alterando os valor das constantes com os valores do vetor original de constantes calibradas
			atribuicao_calc (vet_ctes);
			if (tempo_atual<DiaP_fim) {
				if ( con_rept <= repeticoes ) {
					status_ctes = RAMPA_PERT;
					con_rept++;
				}
				else {
					DiaP_ini += freq_pert;							//Atualiza o diapini para o pr�ximo dia de perturba��o //Note que se deve obede�er FREQ>2*Deltat_pert 
					status_ctes = N_PERTURBADO;						//Altera a situacao das constantes
					con_rept = 1;
				}
			}
			else status_ctes = RESTAURADO;					//Altera a situacao das constantes para restaurado
		} else if ( freq_pert==0 && tempo_atual >= DiaP_fim+deltat_rest ) {	//Se terminou o per�odo da rampa no caso cont�nuo
			atribuicao_ctes (vet_ctes);							//Em caso positivo, restaura o sistema alterando os valor das constantes com os valores do vetor original de constantes calibradas
			atribuicao_calc (vet_ctes);
			status_ctes = RESTAURADO;
		}
	}
	
	vet_info[46] = con_rept;
}

#endif

