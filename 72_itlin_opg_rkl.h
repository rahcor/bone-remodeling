// Rotinas de solu��o do Metodo Iterativo linear para calculo de OPG e RANKL
//Sistema n�o linear de 2eqs e 2incognitas: C_RKL e C_OPG
#ifndef _REMODELAGEM
#include "00_var_common.h"
#include <math.h>
#include <stdlib.h>
#define	_REMODELAGEM

void itlin_OPG_RKL (void)
{
	double	OPG1, C_RKL1;
	double	erroOPG, erroRKL, erroOPG1=0, erroRKL1=0, erro;	
	int		con=1;				//contador
	
	//Comandos de escrita em arquivo
		FILE	*ofp;
		char	str[100];

		if (flagesc!=0) {
			if (flagesc==1) sprintf (str, "out_41_itlin_OPG_RKL_erk1/out_41_itlin_OPG_RKL_calibracao.txt");
			else if(flagesc==2)	sprintf (str, "out_41_itlin_OPG_RKL_erk1/out_41_itlin_OPG_RKL_tempo%.5lf.txt", tempo_atual);
			//else if(flageesc==2)	sprintf (str, "out_41_itlin_OPG_RKL_erk%d/out_41_itlin_OPG_RKL_tempo%.1lf.txt", erk+1, tempo_atual/h);
			ofp = fopen( str , "w");
		
			fprintf (ofp, "iteracao OPG \t\t RKL \t\t erroOPG \t erroRKL \t erro_max\n");	//Escreve cabecalho do arquivo output
			fprintf (ofp, "ini \t %.3E \t %.3E\n", OPG, C_RKL);		//Escreve os valores iniciais no arquivo
		}
	
	//M�todo num�rico		
	erro=fmin(C_RKL,OPG)*errotgt+1;		//'Inicializa' o valor do erro. Forca a entrada no la�o while
	while ( erro>(fmin(C_RKL,OPG)*errotgt) && con<it_max ) {		//Enquanto erro for menor que o desejado e n�o for atingido o n�mero maximo de iteracoes
		    //Solu��o das Equa��es de itera��o das popula��es 
			OPGtot = (1+Ka_RL_OPG*C_RKL);
			if ( OPG*OPGtot>C_OPG_max )
				OPG1 = (P_OPG) / (OPGtot * D_opg);
			else
				OPG1 = (Beta_opg_max+P_OPG) / (OPGtot * (D_opg+Beta_opg_max/C_OPG_max));
										
			RKLtot = (1 + Ka_RL_OPG*OPG + Ka_RL_R*C_RANK);
			if ( C_RKL*RKLtot>RKLmax )
				C_RKL1 = (Beta_rkl_mech + P_RANKL) / (RKLtot * D_rankl);
			else
				C_RKL1 = (Beta_rkl_max + Beta_rkl_mech + P_RANKL) / (RKLtot * (D_rankl + Beta_rkl_max/RKLmax));
		
			//Verifica��o de valores v�lidos
			if ( OPG1<0 )
				OPG1 = 0;
			if ( C_RKL1<0 )
				C_RKL1 = 0;
			
		    //C�lculo do erro
		    erroOPG = erroOPG1;
		    erroRKL = erroRKL1;
			erroOPG1 = fabs((OPG1-OPG)/fmax(1,OPG1));		//Calcula o erro relativo entre os parametros
			erroRKL1 = fabs((C_RKL1-C_RKL)/fmax(1,C_RKL1));
		    erro = fmax(erroOPG, fmax(erroRKL, fmax(erroOPG1, erroRKL1)));	//Acha a popula��o com maior erro
		
	    //Mostra a itera��o e o erro
//			printf ("\rIteracao = %d \t\t Erro = %.3E", con, erro);
	    //Escreve em arquivo os valores de id_itera��o, Popula��es k1, erros e erro_max
	    if (flagesc!=0) fprintf (ofp, "%d \t %.3E \t %.3E \t %.3E \t %.3E \t %.3E\n", con, OPG, C_RKL, erroOPG1, erroRKL1, erro);

	    //Atualiza os valores das populacoes para a pr�xima itera��o
	    OPG = OPG1;
	    C_RKL = C_RKL1;
	    con++;	//Incremento do contador de iteracoes	
	}//fim while ( erro>errotgt & con<=it_max )
	
	//Escreve no inicio do arquivo os principais par�metros: popula��es calibradas, no de itera��es, errotgt e erro final
	if (flagesc!=0) {
		//	rewind(ofp);
		if (con==it_max)
			fprintf (ofp, "\nNao foi possivel calibrar: numero maximo de iteracoes atingido!");
		fprintf (ofp, "\n\nOPG = %.20E\n", OPG);
		fprintf (ofp, "C_RKL = %.20E\n", C_RKL);
		fprintf (ofp, "Numero total de iteracoes = %d\n", con);
		fprintf (ofp, "Erro obtido = %.3E\n", erro);
		fprintf (ofp, "Erro desejado = %.3E\n", (fmin(C_RKL,OPG)*errotgt));
		fflush (ofp);
		fclose (ofp);
	}

	//Atribui as informacoes que ser�o escritas ao aqruivo sumario de informacoes do metodo
	if (con<it_max) {
		vet_info[0] = OPG;
		vet_info[1] = C_RKL;
		vet_info[2] = con;
		vet_info[3] = erro;
		vet_info[4] = (fmin(C_RKL,OPG)*errotgt);
	}

	//Se for atingido o numero maximo permitido de iteracoes, � mostrada uma mensagem de erro e sai do programa.
	if (con==it_max){
		printf ("\nNao foi possivel resolver o sistema OPG_RKL: numero maximo de iteracoes atingido!");
		printf ("\n\nPressione qualquer tecla para continuar o programa...");
		getchar();
		//exit(0);
		vet_info[0] = 0;
		vet_info[1] = 0;
		vet_info[2] = 0;
		vet_info[3] = 0;
		vet_info[4] = 0;
	}
		
}

#endif


