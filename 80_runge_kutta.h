// Rotina que aplica o m�todo nunm�rico de Runge-Kutta de 4� ordem.
#ifndef _RUNGE_KUTTA_H
#include "00_var_common.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "12_modelo.h"
#include "35_volume.h"
#define	_RUNGE_KUTTA_H

void runge_kutta (void)
{	
	double	tempo_atual_aux, C_OBp_aux, C_OBa_aux, C_OCa_aux, f_vas_aux;	//Vari�veis para armazenar o valor de C_OXx_i antes da aplica��o do m�todo
	double	k[4], l[4], m[4], p[4];											//Paramteros de runge-kutta (abreviado RK)
	int		con;
//	int		erk;															//contador para percorrer todos 4 'estados' do m�todo de runge-kutta
	
	tempo_atual_aux = tempo_atual;
	C_OBp_aux = C_OBp_i;
	C_OBa_aux = C_OBa_i;
	C_OCa_aux = C_OCa_i;
	f_vas_aux = f_vas_i;
			
	//Primeiro 'estado' de Runge-kutta j� foi resolvido: observar ordem da chamada da funcao 'runge_kutta' em 'main.c'.
		k[0] = dC_OBp;
		l[0] = dC_OBa;
		m[0] = dC_OCa;
		p[0] = df_vas;
	
	//Termos 2, 3 e 4 do m�todo de RK
	for (erk=1; erk<4 ; erk++)		//Note que erk inicia em 1 devido a essa ser a posi��o dos vetores k,l e m referentes aos termos 2 do metodo de RK.
	{	
		tempo_atual = tempo_atual_aux + (0.25*erk*erk-0.75*erk+1)*h;
		C_OBp_i = C_OBp_aux + (0.25*erk*erk-0.75*erk+1)*h*k[erk-1];		// Note que f(erk)=0.25*erk^2-0.75*erk+1 possui a seguinte caracteristica: f(1)=0.5, f(2)=0.5, f(3)=1. 
		C_OBa_i = C_OBa_aux + (0.25*erk*erk-0.75*erk+1)*h*l[erk-1];		//		Com isso � poss�vel agrupar todos os 3 �ltimos termos de runge-kutta num mesmo la�o.
		C_OCa_i = C_OCa_aux + (0.25*erk*erk-0.75*erk+1)*h*m[erk-1];
		f_vas_i	= f_vas_aux	+ (0.25*erk*erk-0.75*erk+1)*h*p[erk-1];

		modelo();

		k[erk] = dC_OBp;
		l[erk] = dC_OBa;
		m[erk] = dC_OCa;
		p[erk] = df_vas;
	}
	
	//Obten��o da solu��o da EDO por RK, ou seja, obten��o de C_OXx_i do intervalo de tempo seguinte
	tempo_atual = tempo_atual_aux + h;
	C_OBp_i = C_OBp_aux + (h/6)*(k[0] + 2*k[1] + 2*k[2] + k[3]);
    C_OBa_i = C_OBa_aux + (h/6)*(l[0] + 2*l[1] + 2*l[2] + l[3]);
    C_OCa_i = C_OCa_aux + (h/6)*(m[0] + 2*m[1] + 2*m[2] + m[3]);
    f_vas_i = f_vas_aux + (h/6)*(p[0] + 2*p[1] + 2*p[2] + p[3]);
    erk		= 0;
    
    //Verifica��o de valores v�lidos fisicamente
    volume();
    if (C_OBp_i<0)				C_OBp_i = 0;
    	else if (Rv>1)			C_OBp_i = C_OBp_aux; 		// Se o volume est� 100% ocupado, o n�mero de c�lulas n�o aumenta
    if (C_OBa_i<0)				C_OBa_i = 0;
    	else if (Rv>1)			C_OBa_i = C_OBa_aux; 		// Se o volume est� 100% ocupado, o n�mero de c�lulas n�o aumenta
    if (C_OCa_i<0)				C_OCa_i = 0;
   		else if (Rv>1)			C_OCa_i = C_OCa_aux; 		// Se o volume est� 100% ocupado, o n�mero de c�lulas n�o aumenta
    if (f_vas_i < 0)			f_vas_i = 0;
		else if (f_vas_i > 1)	f_vas_i = 1;
		
	//Atribui as informacoes que ser�o escritas ao vetor de informacoes do metodo
	for (con=0;con<4;con++)	vet_info[5+con] = k[con];
	vet_info[9] = (h/6)*(k[0] + 2*k[1] + 2*k[2] + k[3]);
	for (con=0;con<4;con++)	vet_info[10+con] = l[con];
	vet_info[14] = (h/6)*(l[0] + 2*l[1] + 2*l[2] + l[3]);
	for (con=0;con<4;con++)	vet_info[15+con] = m[con];
	vet_info[19] = (h/6)*(m[0] + 2*m[1] + 2*m[2] + m[3]);
	for (con=0;con<4;con++)	vet_info[20+con] = p[con];
	vet_info[24] = (h/6)*(p[0] + 2*p[1] + 2*p[2] + p[3]);
}

#endif
