// Rotinas de solu��o do metodo itlin para determinacao de OPG e RANKL
#ifndef _REMODELAGEM
#include "00_var_common.h"
#include "35_volume.h"
#include "60_estimulo_mec.h"
#include "65_bmd.h"
#include "70_remodelagem.h"
#include <math.h>
#include <stdlib.h>
#define	_REMODELAGEM


void modelo (void)
{
	//C�lculo da fra��o de volume f_bm
	por_i = f_vas_i + f_lac;
	f_bm_i = 1 - f_vas_i;
	f_exlacbm_i = 1 - por_i;		// que eh igual a f_exlacbm_i = f_bm_i - f_lac;
	volume();

	//C�lculo da BMD com delay de mineralizacao
	bmd();

	//C�lculo da matriz de Rigidez
	int	con;
	for (con=0 ; con<21 ; con++)								//Para todos os 21 termos da matriz de rigidez
		c_cort[con] = f_exlacbm_i*c_bm[con] + por_i*c_vas[con];	//A rigidez do cortical � a soma ponderada das rigidez dos materiais constituintes ponderados por suas fra��es volumetricas (Modelo de Voigt para comp�sitos)

	//C�lculo do est�mulo mec�nico de remodelamento (60_estimulo_mec.h)
	estimulo_mec();

	//Solu��o do modelo de remodelagem
		//Calcula popula��o normalizada
			C_OBp_n_i = C_OBp_i / C_OBp_ini;
			C_OBa_n_i = C_OBa_i / C_OBa_ini;
			C_OCa_n_i = C_OCa_i / C_OCa_ini;
		//C�lculo dos par�metros mec�nicos de remodelamento	(70_remodelagem.h)
		remodelagem_biomecanica();
		//Solu��o das Equa��es implicitas (Equa��es de est�mulo ou repress�o) do modelo de remodelagem (70_remodelagem.h)
		remodelagem_implicitas();		//
		//Solu��o das fun��es das edos (Equa��es da din�mica das popula��es celulares) do modelo de remodelagem (70_remodelagem.h)
		remodelagem_dinamica();

}


#endif


