// Rotina de leitura do valores dos arquivos de input.
#ifndef _LEITURA
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "00_var_common.h"
#include "22_atribuicao.h"
#define	_LEITURA

void leitura (void)
{
	FILE		*ifp;					//Variavel do ponteiro do arquivo de entrada
	char		eol='\0';				//Variavel usada para indicar fim de uma linha - end of line
	int			con;//, aux; 				//con=Contador, aux= vari�vel auxiliar para identificar presen�a ou ausencia do arquivo input_calib.txt
		
	//Abre arquivo
	ifp = fopen("input_calib.txt", "r");//Abertura do arquivo com constantes j� calibradas
	if (ifp == NULL) {					//Se o arquivo input_calib n�o existir
		ifp = fopen("input.txt", "r"); 		//Abertura do arquivo de dados de entrada
		if (ifp == NULL) {						//Se o arquivo input n�o existir
			printf("No input file!\nVerifique o arquivo de entrada...\n\n");	//Avisa o usuario
			exit(0);							//e sai do programa
		}	
		else {									//Se o arquivo input existir
			printf ("Arquivo input.txt aberto.\n");
//			aux=0;								//Variavel auxiliar recebe valor 0
		}
	}
	else {								//Se o arquivo input_calib existir
		printf ("Arquivo input_calib.txt aberto.\n");
//		aux=1;							//Variavel auxiliar recebe o valor 1
	}
	
	//Leitura dos valores e nomes das constantes com armazenamento no vetor vet_ctes do tipo struct 'tipo_cte'
	for (nlin=0 ; nlin<nlin_max ;nlin++) {					//Enquanto n�o atingir o numero total de par�metros permitidos
		if (eol!= EOF) {									//Se n�o for o final do arquivo
			fscanf (ifp, "%f %s", &vet_ctes[nlin].valor, vet_ctes[nlin].nome);				//Armazene o valor e o nome nas suas respectivas posicoes no vetor
			do {eol=fgetc(ifp);} while ( eol != '\n' && eol!=EOF );							//Vai para a pr�xima linha ou final do arquivo
			printf ("Linha %2d: %.3E \t %s\n", nlin+1, vet_ctes[nlin].valor, vet_ctes[nlin].nome);	//Debug: Imprime todo o vetor vet_ctes.
		}
		else												//Se for o final do arquivo
			break;												//sai do la�o
	}
	
	fclose (ifp);		//Fechamento do arquivo
	
	//Busca Kform ou Kres para definir configuracao de input
	printf ("\n>Busca Kform ou Kres para definir configuracao de input.\n");
	for (con=0; con<nlin ; con++) {
		if ( strcmp("Kform",vet_ctes[con].nome) ==0 ) {		//strcmp:Se valor for 0 as strings s�o iguais.
			printf ("Operando com Kform de input.\n");
			Kipt = iptKform;								//Altera a sitacao da variavel 'Kipt' para o c�digo de Kform
		}
		else if ( strcmp("Kres", vet_ctes[con].nome) ==0 ) {	//strcmp:Se valor for 0 as strings s�o iguais.
			printf ("Operando com Kres de input.\n");
			Kipt = iptKres;
		}
	}
	
	//************Leitura das perturbacoes************
	ifp = fopen("input_pert.txt", "r");		 //Abre arquivo
	if (ifp == NULL) {							//Se n�o for encontrado arquivo de perturbacao com nome input_pert.txt
		printf("\n>>>No input_pert file!\n");		//Mostra mensagem
		status_ctes = SEM_PERTURB;					//Altera a situacao das constantes para simulacao sem perturbacao
	}
	else {										//Se houver arquivo de perturbacao com nome input_pert.txt
		status_ctes = N_PERTURBADO;					//Altera a situacao das constantes para n�o perturbadas
		printf ("\n>>>Arquivo de input_pert aberto.\n");
		eol = '\0'; 								//inicializa eol
		//Leitura dos tipos, valores e nomes das perturba��es
		for (nlin_pert=0 ; nlin_pert<nlin_max ;nlin_pert++) {					//Enquanto n�o atingir o numero total de par�metros permitidos
			if (eol!= EOF) {													//Se n�o for o final do arquivo
				fscanf (ifp, "%s %f %s", vet_ctes_pert_lidos[nlin_pert].tipo, &vet_ctes_pert_lidos[nlin_pert].valor, vet_ctes_pert_lidos[nlin_pert].nome);	//L� o tipo, valor e nome e os armazena no vetor de perturba��es lidas
				do {eol=fgetc(ifp);} while ( eol!='\n' && eol!=EOF );			//Vai para a pr�xima linha  ou at� o fim do arquivo
				printf ("Linha %2d:%s %.3E \t %s\n", nlin_pert+1, vet_ctes_pert_lidos[nlin_pert].tipo, vet_ctes_pert_lidos[nlin_pert].valor, vet_ctes_pert_lidos[nlin_pert].nome);	//Debug: Imprime todo o vetor vet_ctes.
			}
			else								//Se for o final do arquivo
				break;								//Sai do la�o
		}
	}
	
	if (ifp != NULL) fclose (ifp);		//Fechamento do arquivo de perturba��o se ele existir
	
	//Busca eps_1 ou sig_1 para definir configuracao de perturb
	printf ("\n>Busca eps_1 ou sig_1 para definir configuracao de perturb.\n");
	for (con=0; con<nlin_pert ; con++) {
		if ( strcmp("eps_1",vet_ctes_pert_lidos[con].nome) ==0 ) {		//strcmp:Se valor for 0 as strings s�o iguais.
			printf ("Operando com epsilon de input.\n");
			Tipt = ipteps;								//Altera a sitacao da variavel 'Kipt' para o c�digo de Kform
		}
		else if ( strcmp("sig_1", vet_ctes_pert_lidos[con].nome) ==0 ) {	//strcmp:Se valor for 0 as strings s�o iguais.
			printf ("Operando com sigma de input.\n");
			Tipt = iptsig;
		}
	}
	
	printf("\n>>>Fim do procedimento de leitura.\n\n");
}


void ins_param_calc (void)			//Inicializa as vari�veis das constantes calculadas no vetor de constantes
{
	//Insere os par�metros do estado de tensao e deformacao na situacao de homeostase mecanica
	if ( Tipt == ipteps ) {
		strcpy (vet_ctes[nlin].nome, "eps_1");
		vet_ctes[nlin].valor = 0;
		nlin++;
		{
			strcpy (vet_ctes[nlin].nome, "eps_2");
			vet_ctes[nlin].valor = 0;
			nlin++;
			strcpy (vet_ctes[nlin].nome, "eps_3");
			vet_ctes[nlin].valor = -330.0E-06;
			nlin++;
			strcpy (vet_ctes[nlin].nome, "eps_4");
			vet_ctes[nlin].valor = 0;
			nlin++;
			strcpy (vet_ctes[nlin].nome, "eps_5");
			vet_ctes[nlin].valor = 0;
			nlin++;
			strcpy (vet_ctes[nlin].nome, "eps_6");
			vet_ctes[nlin].valor = 0;
			nlin++;
		}
	}
	else if ( Tipt == iptsig ) {
		strcpy (vet_ctes[nlin].nome, "sig_1");
		vet_ctes[nlin].valor = 0;
		nlin++;
		{
			strcpy (vet_ctes[nlin].nome, "sig_2");
			vet_ctes[nlin].valor = 0;
			nlin++;
			strcpy (vet_ctes[nlin].nome, "sig_3");
			vet_ctes[nlin].valor = 30.0E+06;
			nlin++;
			strcpy (vet_ctes[nlin].nome, "sig_4");
			vet_ctes[nlin].valor = 0;
			nlin++;
			strcpy (vet_ctes[nlin].nome, "sig_5");
			vet_ctes[nlin].valor = 0;
			nlin++;
			strcpy (vet_ctes[nlin].nome, "sig_6");
			vet_ctes[nlin].valor = 0;
			nlin++;
		}
	}
	//Insere os par�mtros a serem calibrados no final do vetor
	strcpy (vet_ctes[nlin].nome, "D_TGFb");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "D_PTH");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "D_rankl");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "D_opg");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "Htb_act_obu");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao	
	nlin++;
	strcpy (vet_ctes[nlin].nome, "Htb_rep_obp");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "Htb_act_oca");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "Hrkl_act_ocp");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "Hpth_act_ob");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "C_OPG_max");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "alfa");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "Beta_rkl_max");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "DOBp");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "DOBu");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;
	strcpy (vet_ctes[nlin].nome, "DOCp");
	vet_ctes[nlin].valor = 0; 	//Receberao valor apos calculo ou calibracao
	nlin++;	
	//Insere os par�mtros de inje��o externa com valor zero no final do vetor
	strcpy (vet_ctes[nlin].nome, "P_TGFb");
	vet_ctes[nlin].valor = 0; 	//Receberao valor no dia da perturbacao	
	nlin++;
	strcpy (vet_ctes[nlin].nome, "P_PTH");
	vet_ctes[nlin].valor = 0; 	//Receberao valor no dia da perturbacao	
	nlin++;
	strcpy (vet_ctes[nlin].nome, "P_OPG");
	vet_ctes[nlin].valor = 0; 	//Receberao valor no dia da perturbacao	
	nlin++;
	strcpy (vet_ctes[nlin].nome, "P_RANKL");
	vet_ctes[nlin].valor = 0; 	//Receberao valor no dia da perturbacao	
	nlin++;		
}

#endif
