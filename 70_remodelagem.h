// Rotinas de calculo das funcoes do modelo de remodelagem ossea
// As equa��es foram divididas em tr�s categorias: biomec�nica, implicitas e din�mica.
//		Fonte: modelo de remodelagem de Scheiner/Pivonka 2013
#ifndef _REMODELAGEM
#include "00_var_common.h"
#include "72_itlin_opg_rkl.h"
#include <math.h>
#include <stdlib.h>
#define	_REMODELAGEM


void remodelagem_biomecanica (void)
{
	//Funcao de estimulo mec�nico a prolifera��o dos pr�-osteoblastos. Acao mec�nica anabolica.
	if	(w < w_f1)						//Zona inativa
		Pi_mech_act_obp = Pi_mech_ref;
	else
	if	(w < w_f2)						//Rampa de subida
		Pi_mech_act_obp = lambda1 * ((w/w_f1) - 1) + Pi_mech_ref;
	else
	if	(w < w_f3)						//Patamar maximo
		Pi_mech_act_obp = Pi_mech_max;
	else
	if	(w < w_f4)						//Rampa de descida
		Pi_mech_act_obp = lambda2 * ((w/w_f3) - 1) + Pi_mech_max;
	else
		Pi_mech_act_obp = Pi_mech_ref;	//Zona inativa
	
	//Taxa de produ��o de RANKL ativada por est�mulo mec�nico. Acao mec�nica catabolica.
	if	(w < w_r1)						//Rampa de descida
		Beta_rkl_mech = kappa1 * (1 - (w/w_r1));
	else
	if	(w < w_r2)						//Zona inativa
		Beta_rkl_mech = 0;
	else
		Beta_rkl_mech = kappa2 * ((w/w_r2) - 1);//Rampa de subida
}


void remodelagem_implicitas (void)
{
	TGFb = (alfa * Kres * C_OCa_i + P_TGFb) / D_TGFb;
	Pi_Tb_act_Obu = TGFb / (Htb_act_obu + TGFb);
	Pi_Tb_rep_Obp = Htb_rep_obp / (Htb_rep_obp + TGFb);
	
	Pi_Tb_act_oca = TGFb / (Htb_act_oca + TGFb);	
	C_PTHR = N_PTHR_OB * (C_OBp_i + C_OBa_i);
	C_PTH = (Beta_PTH + P_PTH) / (D_PTH * (1 + Ka_PTH_R * C_PTHR));
	C_PTH_PTR = Ka_PTH_R * C_PTH * C_PTHR;
	Pi_pth_act_ob = C_PTH_PTR / (Hpth_act_ob + C_PTH_PTR);
	Pi_pth_rep_ob = Hpth_rep_ob / (Hpth_rep_ob + C_PTH_PTR);
	
	Beta_opg_max = p_opg_ob * (C_OBp_i + C_OBa_i) * Pi_pth_rep_ob;
	RKLmax = N_RKL_OB * (C_OBp_i + C_OBa_i) * Pi_pth_act_ob;

	OPGtot = (1+Ka_RL_OPG*C_RKL);
	OPG = (Beta_opg_max+P_OPG) / (OPGtot * (D_opg+Beta_opg_max/C_OPG_max));								//Chute inicial de OPG
	RKLtot = (1 + Ka_RL_OPG*OPG + Ka_RL_R*C_RANK);
	C_RKL = (Beta_rkl_max + Beta_rkl_mech + P_RANKL) / (RKLtot * (D_rankl + Beta_rkl_max/RKLmax));		//Chute inicial de RKL	
	itlin_OPG_RKL ();																//Solucao de OPG e C_RKL atravez do metodo iterativo linear
		
	C_RKL_RK = Ka_RL_R * C_RKL * C_RANK;
	Pi_rkl_rk_act_ocp = C_RKL_RK / (Hrkl_act_ocp + C_RKL_RK);
}

void remodelagem_dinamica (void)
{
	dC_OBp = (DOBu * C_OBu * Pi_Tb_act_Obu) + (POBp * C_OBp_i * Pi_mech_act_obp) - (DOBp * C_OBp_i * Pi_Tb_rep_Obp);
	dC_OBa = (DOBp * C_OBp_i * Pi_Tb_rep_Obp) - (AOBa * C_OBa_i);
	dC_OCa = (DOCp * C_OCp * Pi_rkl_rk_act_ocp) - (AOCa * C_OCa_i * Pi_Tb_act_oca);
	df_vas = Kres * C_OCa_i - Kform * C_OBa_i;
}



#endif


