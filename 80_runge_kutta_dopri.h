// Rotina que aplica o m�todo nunm�rico de Runge-Kutta de 4� ordem.
#ifndef _RUNGE_KUTTA_DOPRI_H
#include "00_var_common.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
//#include <windows.h>
#include "12_modelo.h"
#include "35_volume.h"
#define	_RUNGE_KUTTA_DOPRI_H

#define TRUE	1
#define FALSE	0

void dopri5 (void)
{
	double	tempo_atual_aux, tempo_final_aux, C_OBp_aux, C_OBa_aux, C_OCa_aux, f_vas_aux;	//Vari�veis para armazenar o valor de C_OXx_i antes da aplica��o do m�todo
	double	k[7], l[7], m[7], p[7], a[6], b[6][6] , errofvas, deltafvas, deltaOBp, deltaOBa, deltaOCa, delta; //Paramteros de runge-kutta-dorman-prince (abreviado RKDopri)
	double	C_OBp_iref, C_OBa_iref, C_OCa_iref, f_vas_iref, erroOBp, erroOBa, erroOCa; // h_min=0.2, h_max=2;    //h_min: 0.008=10min
	double	KSaux_OBp, KSaux_OBa, KSaux_OCa, KSaux_fvas;		//Vari�veis auxiliares no algoritmo de soma de Kahan para calculo do erro c=acumulado
	int		TOBp, TOBa, TOCa, Tfvas, errogrande;
	int		con, conh=0;
//	int		erk;															//contador para percorrer todos 4 'estados' do m�todo de runge-kutta

	a[0] = 1.0/5.0;			 a[1] = 3.0/10.0;			 a[2] = 4.0/5.0;			a[3] = 8.0/9.0;				a[4] = 1.0;				a[5] = 1.0;

	b[0][0] = 1.0/5.0;		 																				for (con=1 ; con<6 ; con++) b[0][con]=0;
	b[1][0] = 3.0/40.0;		 b[1][1] = 9.0/40.0; 															for (con=2 ; con<6 ; con++)	b[1][con]=0;
	b[2][0] = 44.0/45.0; 	 b[2][1] =-56.0/15.0;	 	b[2][2] = 32.0/9.0; 								for (con=3 ; con<6 ; con++)	b[2][con]=0;
	b[3][0] = 19372.0/6561.0;b[3][1] =-25360.0/2187.0;	b[3][2] = 64448.0/6561;	 	b[3][3] =-212.0/729.0;		b[3][4] = 0;			b[3][5] = 0;
	b[4][0] = 9017.0/3168.0; b[4][1] =-355.0/33.0; 		b[4][2] = 46732.0/5247.0; 	b[4][3] = 49.0/176.0; 		b[4][4] =-5103.0/18656.0;b[4][5] = 0;
	b[5][0] = 35.0/384.0;	 b[5][1] = 0; 				b[5][2] = 500.0/1113.0; 	b[5][3] = 125.0/192.0; 		b[5][4] =-2187.0/6784.0;b[5][5] = 11.0/84.0;

	tempo_atual_aux = tempo_atual;
	C_OBp_aux = C_OBp_i;
	C_OBa_aux = C_OBa_i;
	C_OCa_aux = C_OCa_i;
	f_vas_aux = f_vas_i;

	switch (status_ctes) {		//Verifica qual regiao de perturbacao est� o tempo atual para determinar o tempo final da regiao
		case N_PERTURBADO:
		   tempo_final_aux = DiaP_ini;
		   break;

		case RAMPA_PERT:
		   tempo_final_aux = DiaP_ini + con_rept*(deltat_pert+deltat_rest)-deltat_rest;
		   break;

		case PERTURBADO:
		   tempo_final_aux = DiaP_fim;
		   break;

		case RAMPA_REST:
			if (freq_pert==0) tempo_final_aux = DiaP_fim+deltat_rest; //Se for o caso continuo
			else tempo_final_aux = DiaP_ini + con_rept*(deltat_pert+deltat_rest);
			break;

		case RESTAURADO:
			tempo_final_aux = tempo_final;
			break;

		default:
			printf("Nao foi possivel determinar valor para variavel tempo_final_aux em 80_runge_kutta_dopri.h ou nao foi encontrado input_pert.txt\n");
			printf("Programa prosseguira utilizando tempo_final_aux = tempo_final\n");
			tempo_final_aux = tempo_final;
			break;
	}

	//Primeiro 'estado' de Runge-kutta j� foi resolvido: observar ordem da chamada da funcao 'runge_kutta_dopri' em 'main.c'.
	//O la�o for inicializa o restante do vetor
		k[0] = dC_OBp;	for (con=1; con<7 ; con++)	k[con]=0;
		l[0] = dC_OBa;	for (con=1; con<7 ; con++)	l[con]=0;
		m[0] = dC_OCa;	for (con=1; con<7 ; con++)	m[con]=0;
		p[0] = df_vas;	for (con=1; con<7 ; con++)	p[con]=0;

	errogrande = TRUE;		//'Inicializa' o valor do erro. Forca a entrada no la�o while

	while ( errogrande==TRUE && conh<it_max ) {		//Enquanto erro for menor que o desejado e n�o for atingido o n�mero maximo de iteracoes
		//Termos 2, 3, 4, 5, 6 e 7 do m�todo de RK Dormand-Prince
		for (erk=0; erk<6 ; erk++)		//Note que erk inicia em 0 devido a essa ser a posi��o dos vetores k,l e m referentes aos termos 2 do metodo de RKF.
		{
			tempo_atual = tempo_atual_aux + a[erk]*h;
//			printf ("\r tempo atual = %.6lf",tempo_atual);
//			Sleep (200);
			C_OBp_i = C_OBp_aux + h*(b[erk][0]*k[0]+b[erk][1]*k[1]+b[erk][2]*k[2]+b[erk][3]*k[3]+b[erk][4]*k[4]+b[erk][5]*k[5]);		// Note que f(erk)=0.25*erk^2-0.75*erk+1 possui a seguinte caracteristica: f(1)=0.5, f(2)=0.5, f(3)=1.
			C_OBa_i = C_OBa_aux + h*(b[erk][0]*l[0]+b[erk][1]*l[1]+b[erk][2]*l[2]+b[erk][3]*l[3]+b[erk][4]*l[4]+b[erk][5]*l[5]);		//		Com isso � poss�vel agrupar todos os 3 �ltimos termos de runge-kutta num mesmo la�o.
			C_OCa_i = C_OCa_aux + h*(b[erk][0]*m[0]+b[erk][1]*m[1]+b[erk][2]*m[2]+b[erk][3]*m[3]+b[erk][4]*m[4]+b[erk][5]*m[5]);
			f_vas_i	= f_vas_aux	+ h*(b[erk][0]*p[0]+b[erk][1]*p[1]+b[erk][2]*p[2]+b[erk][3]*p[3]+b[erk][4]*p[4]+b[erk][5]*p[5]);

			modelo();

			k[erk+1] = dC_OBp;
			l[erk+1] = dC_OBa;
			m[erk+1] = dC_OCa;
			p[erk+1] = df_vas;
		}

		//Obten��o do delta da solu��o da EDO por RKF, ou seja, obten��o de C_OXx_i do intervalo de tempo seguinte
		C_OBp_i = 35.0/384.0*k[0] + 0 + 500.0/1113.0*k[2] + 125.0/192.0*k[3] - 2187.0/6784.0*k[4] + 11.0/84.0*k[5];
	    C_OBa_i = 35.0/384.0*l[0] + 0 + 500.0/1113.0*l[2] + 125.0/192.0*l[3] - 2187.0/6784.0*l[4] + 11.0/84.0*l[5];
	    C_OCa_i = 35.0/384.0*m[0] + 0 + 500.0/1113.0*m[2] + 125.0/192.0*m[3] - 2187.0/6784.0*m[4] + 11.0/84.0*m[5];
	    f_vas_i = 35.0/384.0*p[0] + 0 + 500.0/1113.0*p[2] + 125.0/192.0*p[3] - 2187.0/6784.0*p[4] + 11.0/84.0*p[5];

		C_OBp_iref = 5179.0/57600.0*k[0] + 0 + 7571.0/16695.0*k[2] + 393.0/640.0*k[3] - 92097.0/339200.0*k[4] + 187.0/2100.0*k[5] + 1.0/40.0*k[6];
	    C_OBa_iref = 5179.0/57600.0*l[0] + 0 + 7571.0/16695.0*l[2] + 393.0/640.0*l[3] - 92097.0/339200.0*l[4] + 187.0/2100.0*l[5] + 1.0/40.0*l[6];
	    C_OCa_iref = 5179.0/57600.0*m[0] + 0 + 7571.0/16695.0*m[2] + 393.0/640.0*m[3] - 92097.0/339200.0*m[4] + 187.0/2100.0*m[5] + 1.0/40.0*m[6];
	    f_vas_iref = 5179.0/57600.0*p[0] + 0 + 7571.0/16695.0*p[2] + 393.0/640.0*p[3] - 92097.0/339200.0*p[4] + 187.0/2100.0*p[5] + 1.0/40.0*p[6];

	    erroOBp = fabs(C_OBp_iref-C_OBp_i);
	    erroOBa = fabs(C_OBa_iref-C_OBa_i);
	    erroOCa = fabs(C_OCa_iref-C_OCa_i);
	    errofvas = fabs(f_vas_iref-f_vas_i);

	    printf ("\r Andamento = %.2lf %% = dia %.1lf de %.0lf >RKF45: conh=%d , h=%.3e\r", (tempo_atual_aux/tempo_final)*100, tempo_atual_aux, tempo_final, conh, h);	//Mostra na tela o andamento da simulacao
//	    Sleep (25);

		Tfvas = (errofvas>f_vas_aux*errotgt)? TRUE:FALSE;
		TOBp = (erroOBp>C_OBp_aux*errotgt)? TRUE:FALSE;
		TOBa = (erroOBa>C_OBa_aux*errotgt)? TRUE:FALSE;
		TOCa = (erroOCa>C_OCa_aux*errotgt)?	TRUE:FALSE;
		errogrande = ( Tfvas==TRUE || TOBp==TRUE )? TRUE:FALSE;
		errogrande = ( errogrande==TRUE || TOBa==TRUE )? TRUE:FALSE;
		errogrande = ( errogrande==TRUE || TOCa==TRUE )? TRUE:FALSE;

		if ( errogrande==FALSE || h==fmin(h_min,tempo_final_aux-tempo_atual_aux)) {
			C_OBp_i = C_OBp_aux + h*(5179.0/57600.0*k[0] + 7571.0/16695.0*k[2] + 393.0/640.0*k[3] - 92097.0/339200.0*k[4] + 187.0/2100.0*k[5] + 1.0/40.0*k[6]);
		    C_OBa_i = C_OBa_aux + h*(5179.0/57600.0*l[0] + 7571.0/16695.0*l[2] + 393.0/640.0*l[3] - 92097.0/339200.0*l[4] + 187.0/2100.0*l[5] + 1.0/40.0*l[6]);
		    C_OCa_i = C_OCa_aux + h*(5179.0/57600.0*m[0] + 7571.0/16695.0*m[2] + 393.0/640.0*m[3] - 92097.0/339200.0*m[4] + 187.0/2100.0*m[5] + 1.0/40.0*m[6]);
		    f_vas_i = f_vas_aux	+ h*(5179.0/57600.0*p[0] + 7571.0/16695.0*p[2] + 393.0/640.0*p[3] - 92097.0/339200.0*p[4] + 187.0/2100.0*p[5] + 1.0/40.0*p[6]);

			//Verifica��o de valores v�lidos fisicamente
			volume();
			if (C_OBp_i<0 || Rv>1)				C_OBp_i = C_OBp_aux; // Se o n�mero de c�lulas for negativo ou o volume for 100% ocupado, o n�mero de c�lulas mant�m-se o mesmo do tempo anterior.
			if (C_OBa_i<0 || Rv>1)				C_OBa_i = C_OBa_aux;
			if (C_OCa_i<0 || Rv>1)				C_OCa_i = C_OCa_aux;
			if (f_vas_i<0 || f_vas_i>(1-f_lac))		f_vas_i = f_vas_aux;

		    //Atribui as informacoes que ser�o escritas ao vetor de informacoes do metodo
			for (con=0;con<7;con++)	vet_info[5+con] = k[con];
			vet_info[12] = h*(5179.0/57600.0*k[0] + 7571.0/16695.0*k[2] + 393.0/640.0*k[3] - 92097.0/339200.0*k[4] + 187.0/2100.0*k[5] + 1.0/40.0*k[6]);
			for (con=0;con<7;con++)	vet_info[13+con] = l[con];
			vet_info[20] = h*(5179.0/57600.0*l[0] + 7571.0/16695.0*l[2] + 393.0/640.0*l[3] - 92097.0/339200.0*l[4] + 187.0/2100.0*l[5] + 1.0/40.0*l[6]);
			for (con=0;con<7;con++)	vet_info[21+con] = m[con];
			vet_info[28] = h*(5179.0/57600.0*m[0] + 7571.0/16695.0*m[2] + 393.0/640.0*m[3] - 92097.0/339200.0*m[4] + 187.0/2100.0*m[5] + 1.0/40.0*m[6]);
			for (con=0;con<7;con++)	vet_info[29+con] = p[con];
			vet_info[36] = h*(5179.0/57600.0*p[0] + 7571.0/16695.0*p[2] + 393.0/640.0*p[3] - 92097.0/339200.0*p[4] + 187.0/2100.0*p[5] + 1.0/40.0*p[6]);
			vet_info[37] = erroOBp; vet_info[38] = erroOBa; vet_info[39] = erroOCa; vet_info[40] = errofvas;
			vet_info[41] = f_vas_aux*errotgt; vet_info[42] = errogrande;
			//vet_info[47] += erroOBp/C_OBp_aux; vet_info[48] += erroOBa/C_OBa_aux; vet_info[49] += erroOCa/C_OCa_aux; vet_info[50] += errofvas/f_vas_aux; //Calculo do erro relativo acumulado direto na variavel de escrita
			//C�lculo do erro acumulado considerando a problem�tica da soma de float precision.
			//Feita com duas op��es: uso da biblioteca MPFR e o algoritmo de Kahan.
				//Biblioteca
					//declara��o de vari�veis globais no padr�o da biblioteca
					//c�lculo do erro acumulado
					//c�pia do resultado com convers�o para double para a vari�vel vet_info para ser escrita
				//Kaham Summation
					//declara��o global dos corretores e vet_info_old para cada tipo. Declara��o aux local.
					old_ac_OBp = vet_info[47]; 					old_ac_OBa = vet_info[48];					old_ac_OCa = vet_info[49];					old_ac_fvas = vet_info[50];
					KSaux_OBp = erroOBp/C_OBp_aux - KSc_OBp; 	KSaux_OBa = erroOBa/C_OBa_aux - KSc_OBa;	KSaux_OCa = erroOCa/C_OCa_aux - KSc_OCa;	KSaux_fvas = errofvas/f_vas_aux - KSc_fvas;
					vet_info[47] = vet_info[47] + KSaux_OBp;	vet_info[48] = vet_info[48] + KSaux_OBa;	vet_info[49] = vet_info[49] + KSaux_OCa;	vet_info[50] = vet_info[50] + KSaux_fvas;
					KSc_OBp = (vet_info[47] - old_ac_OBp) - KSaux_OBp;	KSc_OBa = (vet_info[48] - old_ac_OBa) - KSaux_OBa;	KSc_OCa = (vet_info[49] - old_ac_OCa) - KSaux_OCa;	KSc_fvas = (vet_info[50] - old_ac_fvas) - KSaux_fvas;
		}

		if ( h==fmin(h_min,tempo_final_aux-tempo_atual_aux )) { //Se o h j� est� em seu valor minimo
				errogrande=FALSE;							//For�a a saida do m�todo para o caso h==minimo
				printf ("\nNao foi possivel resolver RK-Dopri com o erro desejado: h atingiu o valor minimo!\n\n");
//				printf ("\n\nPressione qualquer tecla para continuar o programa...");
//				getchar();
		}

		//calcula novo h
		deltafvas = 0.84 * pow (f_vas_aux*errotgt/errofvas, 0.25);
		deltaOBp = 0.84 * pow (C_OBp_aux*errotgt/erroOBp, 0.25);
		deltaOBa = 0.84 * pow (C_OBa_aux*errotgt/erroOBa, 0.25);
		deltaOCa = 0.84 * pow (C_OCa_aux*errotgt/erroOCa, 0.25);

		delta = fmin(fmin(deltaOBp,fmin(deltaOBa,deltaOCa)),deltafvas);

		h = fmin(fmin(delta*h,h_max),tempo_final_aux-tempo_atual_aux);

		if (h<h_min) 	h = fmin(h_min,tempo_final_aux-tempo_atual_aux);

		conh++;

		if ( errogrande==FALSE ) {
			//Atribui as informacoes que ser�o escritas ao vetor de informacoes do metodo
			vet_info[43] = conh; vet_info[44] = h; vet_info[45] = tempo_final_aux;
		}

		if (conh==it_max) {
			printf ("\nNao foi possivel resolver RK-Dopri com o erro desejado: numero maximo de iteracoes atingido!\n\n");
//			printf ("\n\nPressione qualquer tecla para continuar o programa...");
//			getchar();
		}

	}

	tempo_atual = tempo_atual_aux + h;
}

#endif
