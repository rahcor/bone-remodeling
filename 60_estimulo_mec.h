// Rotina que calcula o est�mulo mec�nico do remodelamento
#ifndef _EST_MECANICO_H
#include "00_var_common.h"
#include <math.h>
#include <stdlib.h>
#define	_EST_MECANICO_H

#define TRUE	1
#define FALSE	0


int busca_na_matriz (int l, int c);	//Fun��o que retorna a posicao do vetor (da matriz simetrica) refrente a linha l e coluna c da matriz completa. Obs: l e c v�o de 1 a 6; primeira posi��o do vetor � [0].
int	elem_diag_normal (int lc);		//Funcao que retorna o id do vetor referente ao elemento da diagonal principal de uma matriz (completa de 36 posi��es). Obs: lc vai de 1 a 6; primeira posi��o do vetor � [0].
int elem_diag_simetrica (int lc);	//Funcao que retorna o id do vetor referente ao elemento da diagonal principal da matriz simetrica (21 posi��es). Obs: lc vai de 1 a 6; primeira posi��o do vetor � [0].
int	conv_ind (int i, int j);		//Funcao que realiza a conversao de indices duplos (com simetria) para os indices de um vetor. Exemplos: c_(ij)(kl) e epsilon_(ij). No caso do tensor c, essa funcao converte c numa matriz.
									//	Obs: i e j v�o de 1 a 3. A funcao retorna indices de 1 a 6.

void estimulo_mec (void)
{	
	int		i, j, k, l;		//contadores i,j,k e l
	double	acm=0;			//acumulador
	
	if ( Tipt == ipteps ) {
		//C�lculo de sigma: sigma_ij=c_cort_ijkl*epsilon_kl
		for (i=1; i<=3; i++) {
			for (j=i; j<=3; j++) {		//Usa-se o fato de sigma ser sim�trico --> j=i
				for (k=1; k<=3; k++) {
					for (l=1; l<=3; l++)
						acm+=  c_cort[busca_na_matriz( conv_ind(i,j) , conv_ind(k,l) )]  *  epsilon[ conv_ind(k,l)-1 ];
				}
				sigma[conv_ind(i,j)-1] = acm;
				acm=0;
			}
		}
	}
	else if ( Tipt == iptsig ) {
		//Metodo de solucao aplicavel somente para materiais sem efeito cruzado de tensao-cisalhamento, ou seja,
		int T1112, T1123, T1131, T2212, T2223, T2231, T3312, T3323, T3331, T1223, T1231, T2331, verificacao;
		
		T1112 = (c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(1,2))] == 0)? TRUE:FALSE;
		T1123 = (c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(2,3))] == 0)? TRUE:FALSE;
		T1131 = (c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(3,1))] == 0)? TRUE:FALSE;
		T2212 = (c_cort[busca_na_matriz(conv_ind(2,2),conv_ind(1,2))] == 0)? TRUE:FALSE;
		T2223 = (c_cort[busca_na_matriz(conv_ind(2,2),conv_ind(2,3))] == 0)? TRUE:FALSE;
		T2231 = (c_cort[busca_na_matriz(conv_ind(2,2),conv_ind(3,1))] == 0)? TRUE:FALSE;
		T3312 = (c_cort[busca_na_matriz(conv_ind(3,3),conv_ind(1,2))] == 0)? TRUE:FALSE;
		T3323 = (c_cort[busca_na_matriz(conv_ind(3,3),conv_ind(2,3))] == 0)? TRUE:FALSE;
		T3331 = (c_cort[busca_na_matriz(conv_ind(3,3),conv_ind(3,1))] == 0)? TRUE:FALSE;
		T1223 = (c_cort[busca_na_matriz(conv_ind(1,2),conv_ind(2,3))] == 0)? TRUE:FALSE;
		T1231 = (c_cort[busca_na_matriz(conv_ind(1,2),conv_ind(3,1))] == 0)? TRUE:FALSE;
		T2331 = (c_cort[busca_na_matriz(conv_ind(2,3),conv_ind(3,1))] == 0)? TRUE:FALSE;
		verificacao = ( T1112==TRUE || T1123==TRUE )? TRUE:FALSE;
		verificacao = ( verificacao==TRUE || T1131==TRUE )? TRUE:FALSE;
		verificacao = ( verificacao==TRUE || T2212==TRUE )? TRUE:FALSE;
		verificacao = ( verificacao==TRUE || T2223==TRUE )? TRUE:FALSE;
		verificacao = ( verificacao==TRUE || T2231==TRUE )? TRUE:FALSE;
		verificacao = ( verificacao==TRUE || T3312==TRUE )? TRUE:FALSE;
		verificacao = ( verificacao==TRUE || T3323==TRUE )? TRUE:FALSE;
		verificacao = ( verificacao==TRUE || T3331==TRUE )? TRUE:FALSE;
		verificacao = ( verificacao==TRUE || T1223==TRUE )? TRUE:FALSE;
		verificacao = ( verificacao==TRUE || T1231==TRUE )? TRUE:FALSE;
		verificacao = ( verificacao==TRUE || T2331==TRUE )? TRUE:FALSE;
		
		if ( verificacao == FALSE ) {
			printf("\n\nCriterio para calculo do estado de deformacao nao atendido!!\n");
			printf("Verifique os zeros da matriz de rigidez.\n\n");
			exit(0);
		}
		else {
			double	cta, ctb, ctc, ctd, cte, ctf, ctg, cth;
			
			cta = c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(1,1))]  /  c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(2,2))];
			ctb = cta * sigma[conv_ind(2,2)-1] - sigma[conv_ind(1,1)-1];
			ctc = cta * c_cort[busca_na_matriz(conv_ind(2,2),conv_ind(2,2))] - c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(2,2))];
			ctd = cta * c_cort[busca_na_matriz(conv_ind(2,2),conv_ind(3,3))] - c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(3,3))];
			cte = c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(1,1))]  /  c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(3,3))];
			ctf = cte * sigma[conv_ind(3,3)-1] - sigma[conv_ind(1,1)-1];
			ctg = cte * c_cort[busca_na_matriz(conv_ind(2,2),conv_ind(3,3))] - c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(2,2))];
			cth = cte * c_cort[busca_na_matriz(conv_ind(3,3),conv_ind(3,3))] - c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(3,3))];
			
			epsilon[conv_ind(3,3)-1] = (ctf*ctc - ctb*ctg) / (cth*ctc - ctd*ctg);
			epsilon[conv_ind(2,2)-1] = (ctb - ctd*epsilon[conv_ind(3,3)-1]) / ctc;
			epsilon[conv_ind(1,1)-1] = (sigma[conv_ind(1,1)-1] - (1/cta)*epsilon[conv_ind(2,2)-1] - (1/cte)*epsilon[conv_ind(3,3)-1]) / c_cort[busca_na_matriz(conv_ind(1,1),conv_ind(1,1))];
			epsilon[conv_ind(1,2)-1] = sigma[conv_ind(1,2)-1] / (2*c_cort[busca_na_matriz(conv_ind(1,2),conv_ind(1,2))]);
			epsilon[conv_ind(2,3)-1] = sigma[conv_ind(2,3)-1] / (2*c_cort[busca_na_matriz(conv_ind(1,2),conv_ind(1,2))]);
			epsilon[conv_ind(3,1)-1] = sigma[conv_ind(3,1)-1] / (2*c_cort[busca_na_matriz(conv_ind(3,1),conv_ind(3,1))]);
		}
	}
	
	
	//Calculo de w: w=0,5*epsilon_ij*sigma_ij
	for (i=1; i<=3; i++) {
		for (j=1; j<=3; j++)
			acm+=  epsilon[ conv_ind(i,j)-1 ]  *  sigma[ conv_ind(i,j)-1 ];
	}
	wcort = 0.5*acm;
	w = wcort/f_bm_i;
}

//////Funcoes///////
int busca_na_matriz (int l, int c)	//Fun��o que retorna a posicao do vetor (da matriz simetrica) refrente a linha l e coluna c da matriz completa. Obs: l e c v�o de 1 a 6; primeira posi��o do vetor � [0].
{
	if (l>c)	//elementos da diagonal inferior
		return (elem_diag_simetrica(c) + (l-c));	//(l-c) � a dist�nica at� o elemento da diagonal na coluna
	else		//diagonal e elementos da diagonal superior
		return (elem_diag_simetrica(l) + (c-l));	//(c-l) � a dist�nica at� o elemento da diagonal na linha
}

int	elem_diag_normal (int lc)		//Funcao que retorna o id do vetor referente ao elemento da diagonal principal de uma matriz (completa de 36 posi��es). Obs: lc vai de 1 a 6; primeira posi��o do vetor � [0].
{
	return ((6+1) * (lc-1));		//Termo de uma progress�o aritm�tica de 'passo' 7 iniciada em 0. O 'passo' � sete, pois percorre os seis elementos de uma linha mais um, para alcan�ar o elemento da diagonal.
}

int elem_diag_simetrica (int lc)	//Funcao que retorna o id do vetor referente ao elemento da diagonal principal da matriz simetrica (21 posi��es). Obs: lc vai de 1 a 6; primeira posi��o do vetor � [0].
{
	return (elem_diag_normal(lc) - (lc-1)*lc/2);	//Elem da diagonal na matriz normal menos soma da progress�o aritm�tica de 'passo' -1 iniciando em 0.
}

int	conv_ind (int i, int j)			//Funcao que realiza a conversao de indices duplos (com simetria) para os indices de um vetor. Exemplos: c_(ij)(kl) e epsilon_(ij). No caso do tensor c, essa funcao converte c numa matriz.
{									//	Obs: i e j v�o de 1 a 3. A funcao retorna indices de 1 a 6.
	int aux = 0;
	
		 if (i==j) 	aux = (i);			// Ou seja, se (i,j)==[(1,1) ou (2,2) ou (3,3)]
	else if (i!=j) {
		 if ( (i+j)==(3) ) aux = (4);	// Ou seja, se (i,j)==[(1,2) ou (2,1)]
	else if ( (i+j)==(5) ) aux = (5);	// Ou seja, se (i,j)==[(2,3) ou (3,2)]
	else if ( (i+j)==(4) ) aux = (6);	// Ou seja, se (i,j)==[(3,1) ou (1,3)]
	}
	
	return (aux);
}


#endif
